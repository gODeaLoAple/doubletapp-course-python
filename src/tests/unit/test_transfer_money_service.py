import pytest

from app.internal.bank.db.models import BankAccount, Transaction
from app.internal.bot.services.exceptions import IncorrectTransferMoneyError, NotEnoughMoneyError
from app.internal.bot.services.transfer_money_service import transfer_money_to_bank_account
from tests.utils.actions import make_favorite
from tests.utils.bank_account_mother import BankAccountMother
from tests.utils.user_mother import UserMother


@pytest.mark.django_db
@pytest.mark.unit
def test_transfer_money_to_bank_account_when_user_is_none():
    user = UserMother.create_user()
    bank_account = BankAccountMother.create_account(user, 1000)

    with pytest.raises(Exception):
        transfer_money_to_bank_account(None, bank_account.id, 100)


@pytest.mark.django_db
@pytest.mark.unit
def test_transfer_money_to_bank_account_when_bank_account_is_none():
    user = UserMother.create_user()
    BankAccountMother.create_account(user, 1000)

    with pytest.raises(Exception):
        transfer_money_to_bank_account(user.id, None, 100)


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.parametrize("money", [-100, 0])
def test_transfer_money_to_bank_account_when_invalid_money(money):
    gen = UserMother.generate_users()
    user = next(gen)
    BankAccountMother.create_account(user, 1000)
    other_user = next(gen)
    other_bank_account = BankAccountMother.create_account(other_user, 100)

    with pytest.raises(IncorrectTransferMoneyError):
        transfer_money_to_bank_account(user.id, other_bank_account.id, money)


@pytest.mark.django_db
@pytest.mark.unit
def test_transfer_money_to_bank_account_when_self_transfer():
    gen = UserMother.generate_users()
    user = next(gen)
    bank_account = BankAccountMother.create_account(user, 1000)

    with pytest.raises(Exception):
        transfer_money_to_bank_account(user.id, bank_account.id, 10)


@pytest.mark.django_db
@pytest.mark.unit
def test_transfer_money_to_bank_account_when_user_has_not_enough_money():
    gen = UserMother.generate_users()
    user = next(gen)
    BankAccountMother.create_account(user, 0)
    other_user = next(gen)
    other_bank_account = BankAccountMother.create_account(other_user, 0)
    make_favorite(user, other_user)

    with pytest.raises(NotEnoughMoneyError):
        transfer_money_to_bank_account(user.id, str(other_bank_account.id), 100)


@pytest.mark.django_db
@pytest.mark.unit
def test_transfer_money_to_bank_account_when_user_has_enough_money():
    gen = UserMother.generate_users()
    user = next(gen)
    bank_account = BankAccountMother.create_account(user, 1000)
    other_user = next(gen)
    other_bank_account = BankAccountMother.create_account(other_user, 0)
    make_favorite(user, other_user)

    transfer_money_to_bank_account(user.id, str(other_bank_account.id), 300)

    assert BankAccount.objects.get(id=bank_account.id).money == 700
    assert BankAccount.objects.get(id=other_bank_account.id).money == 300
    assert Transaction.objects.get(sender_account=bank_account, recipient_account=other_bank_account).money == 300
