import datetime

import pytest
from django.utils import timezone
from freezegun import freeze_time

from app.internal.auth.db.models import IssuedToken
from app.internal.auth.utils import generate_refresh_token, try_parse_token
from app.internal.bot.services.auth_service import login, refresh, update_password
from app.internal.bot.services.exceptions import (
    AuthenticatedUserNotFoundError,
    IncorrectUserPasswordError,
    RefreshTokenExpiredError,
    RefreshTokenHasBeenRevokedError,
    RefreshTokenNotRecognizedError,
)
from app.internal.users.db.models import AuthenticatedUser
from config import settings
from tests.utils.user_mother import UserMother


@pytest.mark.django_db
@pytest.mark.unit
def test_update_password_when_user_not_exists():
    with pytest.raises(AuthenticatedUser.DoesNotExist):
        update_password(0, "a", "b")


@pytest.mark.django_db
@pytest.mark.unit
def test_update_password_when_wrong_password():
    password = "pwd"
    user = UserMother.create_user(password=password)

    with pytest.raises(IncorrectUserPasswordError):
        update_password(user.telegram_id, password + "a", password + "b")


@pytest.mark.django_db
@pytest.mark.unit
def test_update_password_when_correct_password():
    password = "pwd"
    new_password = "newpass"
    user = UserMother.create_user(password=password)

    update_password(user.telegram_id, new_password, password)

    assert AuthenticatedUser.objects.filter(id=user.id).get().check_password(new_password)


@pytest.mark.django_db
@pytest.mark.unit
def test_login_when_user_not_exists():
    with pytest.raises(AuthenticatedUserNotFoundError):
        login("not_exist", "password")


@pytest.mark.django_db
@pytest.mark.unit
def test_login_when_incorrect_password():
    password = "pwd"
    user = UserMother.create_user(password=password)

    with pytest.raises(AuthenticatedUserNotFoundError):
        login(user.username, password + "a")


@pytest.mark.django_db
@pytest.mark.unit
def test_login_when_correct_password():
    password = "pwd"
    user = UserMother.create_user(password=password)

    access_token, refresh_token = login(user.username, password)

    assert IssuedToken.objects.filter(user=user, revoked=False).count() == 1
    assert access_token
    assert refresh_token


@pytest.mark.django_db
@pytest.mark.unit
def test_login_several_times_when_correct_password():
    password = "pwd"
    user = UserMother.create_user(password=password)

    login(user.username, password)
    login(user.username, password)
    access_token, refresh_token = login(user.username, password)

    assert IssuedToken.objects.filter(user=user, revoked=True).count() == 2
    assert IssuedToken.objects.filter(user=user, revoked=False).count() == 1
    assert access_token
    assert refresh_token


@pytest.mark.django_db
@pytest.mark.unit
def test_refresh_when_refresh_token_is_none():
    with pytest.raises(RefreshTokenNotRecognizedError):
        refresh(None)


@pytest.mark.django_db
@pytest.mark.unit
def test_refresh_when_token_not_found():
    refresh_token = generate_refresh_token(0, timezone.timedelta(seconds=100))

    with pytest.raises(RefreshTokenNotRecognizedError):
        refresh(refresh_token)


@pytest.mark.django_db
@pytest.mark.unit
def test_refresh_when_refresh_token_incorrect():
    with pytest.raises(RefreshTokenNotRecognizedError):
        refresh("refresh_token")


@pytest.mark.django_db
@pytest.mark.unit
def test_refresh_when_refresh_token_revoked():
    password = "password"
    user = UserMother.create_user(password=password)

    schema = login(user.username, password)
    login(user.username, password)

    with pytest.raises(RefreshTokenHasBeenRevokedError):
        refresh(schema.refresh_token)
    assert not IssuedToken.objects.filter(user=user, revoked=False).exists()


@pytest.mark.django_db
@pytest.mark.unit
def test_refresh_when_refresh_token_expired():
    password = "password"
    user = UserMother.create_user(password=password)

    schema = login(user.username, password)
    with freeze_time(datetime.datetime.now()) as freezer:
        delay = datetime.timedelta(seconds=settings.JWT_REFRESH_TOKEN_EXPIRED) + datetime.timedelta(seconds=10)
        freezer.tick(delta=delay)
        with pytest.raises(RefreshTokenExpiredError):
            refresh(schema.refresh_token)
    assert not IssuedToken.objects.filter(user=user, revoked=False).exists()


@pytest.mark.django_db
@pytest.mark.unit
def test_refresh_when_refresh_token_correct():
    password = "password"
    user = UserMother.create_user(password=password)

    schema = login(user.username, password)
    refresh(schema.refresh_token)

    success, token = try_parse_token(schema.refresh_token)
    assert success
    assert IssuedToken.objects.filter(id=token["id"]).get().revoked
    assert IssuedToken.objects.filter(user=user, revoked=False).count() == 1
