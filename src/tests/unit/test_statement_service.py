import pytest
from django.utils import timezone

from app.internal.bank.db.models import BankAccount
from app.internal.bot.services.statement_service import get_statement
from tests.utils.actions import add_transactions, sort_transactions
from tests.utils.bank_account_mother import BankAccountMother
from tests.utils.card_mother import CardMother
from tests.utils.transaction_mother import TransactionMother
from tests.utils.user_mother import UserMother

start_date = timezone.now() - timezone.timedelta(30)


@pytest.mark.django_db
@pytest.mark.unit
def test_get_statement_when_user_not_exists():
    # noinspection PyTypeChecker
    with pytest.raises(BankAccount.DoesNotExist):
        get_statement(0, "1", start_date)


@pytest.mark.django_db
@pytest.mark.unit
def test_get_statement_when_user_has_not_bank_account():
    user = UserMother.create_user()

    # noinspection PyTypeChecker
    with pytest.raises(BankAccount.DoesNotExist):
        get_statement(user.id, "1", start_date)


@pytest.mark.django_db
@pytest.mark.unit
def test_get_statement_when_someone_else_has_this_bank_account():
    gen = UserMother.generate_users()
    user = next(gen)
    other = next(gen)
    other_account = BankAccountMother.create_account(other, 1000)

    # noinspection PyTypeChecker
    with pytest.raises(BankAccount.DoesNotExist):
        get_statement(user.id, str(other_account.id), start_date)


@pytest.mark.django_db
@pytest.mark.unit
def test_get_statement_when_someone_else_has_this_card():
    gen = UserMother.generate_users()
    user = next(gen)
    other = next(gen)
    other_account = BankAccountMother.create_account(other, 1000)
    other_card = CardMother.create_card(other_account, "1111222233334444")

    # noinspection PyTypeChecker
    with pytest.raises(BankAccount.DoesNotExist):
        get_statement(user.id, str(other_card.name), start_date)


@pytest.mark.django_db
@pytest.mark.unit
def test_get_statement_by_account_id():
    user, account, cards, transactions = _create_success_case()

    statement_account_id, statement_cards, statement_transactions = get_statement(user.id, str(account.id), start_date)

    assert statement_account_id == account.id
    assert set(statement_cards) == set(cards)
    assert sort_transactions(map(TransactionMother.to_dto, transactions)) == sort_transactions(statement_transactions)


@pytest.mark.django_db
@pytest.mark.unit
def test_get_statement_by_card_name():
    user, account, cards, transactions = _create_success_case()

    statement_account_id, statement_cards, statement_transactions = get_statement(user.id, cards[0], start_date)

    assert statement_account_id == account.id
    assert set(statement_cards) == set(cards)
    assert sort_transactions(map(TransactionMother.to_dto, transactions)) == sort_transactions(statement_transactions)


def _create_success_case():
    gen = UserMother.generate_users()
    user = next(gen)
    account = BankAccountMother.create_account(user, 1000)
    other = next(gen)
    other_account = BankAccountMother.create_account(other)
    cards, transactions = add_transactions(user, account, other, other_account)
    return user, account, cards, transactions
