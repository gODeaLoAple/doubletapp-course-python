import pytest

from app.internal.bank.db.models import Card
from app.internal.bot.services.card_service import get_card_balance
from app.internal.bot.services.exceptions import UserCardNotFoundError
from tests.utils.bank_account_mother import BankAccountMother
from tests.utils.card_mother import CardMother
from tests.utils.user_mother import UserMother


@pytest.mark.django_db
@pytest.mark.unit
def test_get_card_balance_when_user_has_no_card():
    user = UserMother.create_user()
    BankAccountMother.create_account(user)

    with pytest.raises(UserCardNotFoundError):
        get_card_balance(user, "0000000000000000")


@pytest.mark.django_db
@pytest.mark.unit
def test_get_card_balance_when_card_is_not_owned_by_user():
    [user, other_user] = UserMother.create_users(2)
    account = BankAccountMother.create_account(other_user)
    card = CardMother.create_card(account)

    with pytest.raises(UserCardNotFoundError):
        get_card_balance(user, card.name)


@pytest.mark.django_db
@pytest.mark.unit
def test_get_card_balance_when_all_ok():
    user = UserMother.create_user()
    money = 10000
    account = BankAccountMother.create_account(user, money=money)
    card = CardMother.create_card(account, name="1111222233334444")

    balance = get_card_balance(user.id, card.name)

    assert balance == money
