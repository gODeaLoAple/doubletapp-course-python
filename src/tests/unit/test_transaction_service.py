import pytest
from django.utils import timezone

from app.internal.bot.services.transactions_service import get_last_transactions, get_transaction_history
from tests.utils.actions import sort_transactions
from tests.utils.bank_account_mother import BankAccountMother
from tests.utils.transaction_mother import TransactionMother
from tests.utils.user_mother import UserMother


@pytest.mark.django_db
@pytest.mark.unit
def test_get_transaction_history_when_username_has_no_transactions():
    user = UserMother.create_user()
    BankAccountMother.create_account(user)

    history = get_transaction_history(user.id)

    assert len(list(history)) == 0


@pytest.mark.django_db
@pytest.mark.unit
def test_get_transaction_history():
    gen = UserMother.generate_users()
    user = next(gen)
    bank_account = BankAccountMother.create_account(user)
    second_user = next(gen)
    second_account = BankAccountMother.create_account(second_user)
    third_user = next(gen)
    third_account = BankAccountMother.create_account(third_user)
    fourth_user = next(gen)
    fourth_account = BankAccountMother.create_account(fourth_user)
    TransactionMother.create_transaction(bank_account, second_account, 100)
    TransactionMother.create_transaction(second_account, fourth_account, 100)
    TransactionMother.create_transaction(third_account, fourth_account, 100)
    TransactionMother.create_transaction(third_account, bank_account, 100)

    usernames = get_transaction_history(user.id)

    assert set(usernames) == {second_user.username, third_user.username}


@pytest.mark.django_db
@pytest.mark.unit
def test_get_last_transactions_when_bank_account_not_exists():
    start_date = timezone.now() - timezone.timedelta(30)
    result = list(get_last_transactions(0, start_date))
    assert not result


@pytest.mark.django_db
@pytest.mark.unit
def test_get_last_transactions_when_has_transactions_only_30_days_ago():
    start_date = timezone.now() - timezone.timedelta(30)
    gen = UserMother.generate_users()
    user = next(gen)
    bank_account = BankAccountMother.create_account(user)
    second_user = next(gen)
    second_account = BankAccountMother.create_account(second_user)
    TransactionMother.create_transaction(bank_account, second_account, 100, start_date - timezone.timedelta(1))
    TransactionMother.create_transaction(second_account, bank_account, 100, start_date - timezone.timedelta(1))

    result = list(get_last_transactions(bank_account.id, start_date))

    assert not result


@pytest.mark.django_db
@pytest.mark.unit
def test_get_last_transactions():
    start_date = timezone.now() - timezone.timedelta(30)
    gen = UserMother.generate_users()
    user = next(gen)
    bank_account = BankAccountMother.create_account(user)
    second_user = next(gen)
    second_account = BankAccountMother.create_account(second_user)
    date = start_date + timezone.timedelta(15)
    transactions = [
        TransactionMother.create_transaction(bank_account, second_account, 100, date),
        TransactionMother.create_transaction(second_account, bank_account, 100, date),
    ]

    result = get_last_transactions(bank_account.id, start_date)

    assert sort_transactions(map(TransactionMother.to_dto, transactions)) == sort_transactions(result)
