import pytest

from app.internal.bot.services.exceptions import FavoriteAlreadyAdded, FavoriteNotAddedError, SelfFavoriteError
from app.internal.bot.services.favorite_service import (
    add_favorite_by_username,
    get_favorites_of_user,
    is_favorite_for,
    remove_favorite_by_username,
)
from tests.utils.user_mother import UserMother


@pytest.mark.unit
@pytest.mark.django_db
def test_add_favorite_by_username_when_user_is_none():
    with pytest.raises(Exception):
        add_favorite_by_username(None, "")


@pytest.mark.unit
@pytest.mark.django_db
def test_add_favorite_by_username_when_favorite_user_already_added():
    gen = UserMother.generate_users()
    user = next(gen)
    other_user = next(gen)

    add_favorite_by_username(user.id, other_user.username)
    with pytest.raises(FavoriteAlreadyAdded):
        add_favorite_by_username(user.id, other_user.username)


@pytest.mark.unit
@pytest.mark.django_db
def test_add_favorite_by_username_when_favorite_user_is_user():
    user = UserMother.create_user()

    with pytest.raises(SelfFavoriteError):
        add_favorite_by_username(user.id, user.username)


@pytest.mark.unit
@pytest.mark.django_db
def test_add_favorite_by_username():
    gen = UserMother.generate_users()
    user = next(gen)
    other_user = next(gen)

    add_favorite_by_username(user.id, other_user.username)

    assert is_favorite_for(user.id, other_user.id)
    assert not is_favorite_for(other_user.id, user.id)


@pytest.mark.unit
@pytest.mark.django_db
def test_remove_favorite_by_username_when_user_is_none():
    with pytest.raises(Exception):
        remove_favorite_by_username(None, "")


@pytest.mark.unit
@pytest.mark.django_db
def test_remove_favorite_by_username_when_favorite_user_is_user():
    user = UserMother.create_user()

    with pytest.raises(FavoriteNotAddedError):
        remove_favorite_by_username(user.id, user.username)


@pytest.mark.unit
@pytest.mark.django_db
def test_remove_favorite_by_username_when_favorite_user_not_added_before():
    gen = UserMother.generate_users()
    user = next(gen)
    other_user = next(gen)

    with pytest.raises(FavoriteNotAddedError):
        remove_favorite_by_username(user.id, other_user.username)


@pytest.mark.unit
@pytest.mark.django_db
def test_remove_favorite_by_username_when_favorite_user_added_before():
    gen = UserMother.generate_users()
    user = next(gen)
    other_user = next(gen)

    add_favorite_by_username(user.id, other_user.username)
    remove_favorite_by_username(user.id, other_user.username)

    assert not is_favorite_for(user.id, other_user.id)
    assert not is_favorite_for(other_user.id, user.id)


@pytest.mark.unit
@pytest.mark.django_db
def test_get_favorites_of_user_when_user_is_none():
    with pytest.raises(Exception):
        get_favorites_of_user(None)


@pytest.mark.unit
@pytest.mark.django_db
def test_get_favorites_of_user_when_user_has_no_favorites():
    user = UserMother.create_user()

    favorites = get_favorites_of_user(user.id).favorites

    assert len(favorites) == 0


@pytest.mark.unit
@pytest.mark.django_db
def test_get_favorites_of_user_when_user_has_one_favorite():
    gen = UserMother.generate_users()
    user = next(gen)
    other_user = next(gen)

    add_favorite_by_username(user.id, other_user.username)
    favorites = get_favorites_of_user(user.id).favorites

    assert list(favorites) == [other_user.username]


@pytest.mark.unit
@pytest.mark.django_db
def test_get_favorites_of_user_when_user_has_several_favorites():
    gen = UserMother.generate_users()
    user = next(gen)
    second_user = next(gen)
    third_user = next(gen)

    add_favorite_by_username(user.id, second_user.username)
    add_favorite_by_username(user.id, third_user.username)
    favorites = get_favorites_of_user(user.id).favorites

    assert set(favorites) == {second_user.username, third_user.username}


@pytest.mark.unit
@pytest.mark.django_db
def test_is_favorite_for_when_user_is_none():
    user = UserMother.create_user()

    with pytest.raises(Exception):
        is_favorite_for(None, user.id)


@pytest.mark.unit
@pytest.mark.django_db
def test_is_favorite_for_when_favorite_user_is_none():
    user = UserMother.create_user()

    with pytest.raises(Exception):
        is_favorite_for(user.id, None)


@pytest.mark.unit
@pytest.mark.django_db
def test_is_favorite_for_when_user_has_another_favorite():
    gen = UserMother.generate_users()
    user = next(gen)
    second_user = next(gen)
    third_user = next(gen)

    add_favorite_by_username(user.id, third_user.username)
    is_favorite = is_favorite_for(user.id, second_user.id)

    assert not is_favorite


@pytest.mark.unit
@pytest.mark.django_db
def test_is_favorite_for_when_favorite_user_has_this_user():
    gen = UserMother.generate_users()
    user = next(gen)
    other_user = next(gen)

    add_favorite_by_username(other_user.id, user.username)
    is_favorite = is_favorite_for(user.id, other_user.id)

    assert not is_favorite


@pytest.mark.unit
@pytest.mark.django_db
def test_is_favorite_for_when_user_has_this_favorite():
    gen = UserMother.generate_users()
    user = next(gen)
    other_user = next(gen)

    add_favorite_by_username(user.id, other_user.username)
    is_favorite = is_favorite_for(user.id, other_user.id)

    assert is_favorite
