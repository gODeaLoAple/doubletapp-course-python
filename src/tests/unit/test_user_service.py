import pytest

from app.internal.bot.services.exceptions import (
    IncorrectPhoneNumberError,
    IncorrectUserDataError,
    PhoneNumberAlreadyExistsError,
)
from app.internal.bot.services.user_service import get_user_info, save_user, set_user_phone
from app.internal.users.db.models import AuthenticatedUser
from tests.utils.user_mother import UserMother


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.parametrize("first_name", [None, "a" * 65])
@pytest.mark.parametrize("last_name", [None])
@pytest.mark.parametrize("username", [None, "a" * 33])
def test_save_user_when_wrong_data(first_name, last_name, username):
    with pytest.raises(IncorrectUserDataError):
        save_user(1, first_name, last_name, username, "password")


@pytest.mark.django_db
@pytest.mark.unit
def test_save_user():
    created = save_user(1, "Boy", "Nextdoorovich", "boychik", "password")

    assert created


@pytest.mark.django_db
@pytest.mark.unit
def test_save_user_when_empty_last_name():
    created = save_user(1, "Boy", "", "boychik", "password")

    assert created


@pytest.mark.django_db
@pytest.mark.unit
def test_save_user_several_times():
    created = save_user(1, "Boy", "", "boychik", "password")
    created2 = save_user(1, "Girl", "", "boychik", "password")

    user = AuthenticatedUser.objects.get(telegram_id=1)
    assert user.first_name == "Girl"
    assert created
    assert not created2


@pytest.mark.django_db
@pytest.mark.unit
def test_set_user_phone_when_user_has_no_phone():
    phone_number = "89208887722"
    user = UserMother.create_user(phone="")

    set_user_phone(user.id, phone_number)

    assert AuthenticatedUser.objects.filter(id=user.id).get().phone == phone_number


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.parametrize("phone_number", [" ", "aaaa", "aabcdef", "-123", "7" * 16])
def test_set_user_phone_when_phone_number_invalid(phone_number):
    user = UserMother.create_user(phone="")

    with pytest.raises(IncorrectPhoneNumberError):
        set_user_phone(user.id, phone_number)


@pytest.mark.django_db
@pytest.mark.unit
def test_set_user_phone_when_user_already_has_phone():
    old_phone_number = "89209998822"
    new_phone_number = "89209998833"
    user = UserMother.create_user(phone=old_phone_number)

    set_user_phone(user.id, new_phone_number)

    assert AuthenticatedUser.objects.filter(id=user.id).get().phone == new_phone_number


@pytest.mark.django_db
@pytest.mark.unit
def test_set_user_phone_when_other_user_already_has_same_phone():
    [user, other_user] = UserMother.create_users(2)

    with pytest.raises(PhoneNumberAlreadyExistsError):
        set_user_phone(user.id, other_user.phone)


@pytest.mark.django_db
@pytest.mark.unit
def test_get_user_info():
    user = UserMother.create_user()

    info = get_user_info(user.id)

    assert info is not None


@pytest.mark.django_db
@pytest.mark.unit
def test_get_user_info_when_user_is_none():
    with pytest.raises(Exception):
        get_user_info(0)
