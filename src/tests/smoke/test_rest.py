from multiprocessing.connection import Client

import pytest

from tests.utils.user_mother import UserMother


@pytest.mark.django_db
@pytest.mark.smoke
def test_me_works(client: Client):
    response = client.get("/api/users/me/")

    assert response.status_code == 401
