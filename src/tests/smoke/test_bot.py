import pytest as pytest
import requests

from config.settings import BOT_TOKEN


@pytest.mark.smoke
def test_bot_works():
    url = f"https://api.telegram.org/bot{BOT_TOKEN}/getMe"
    response = requests.get(url)
    assert response.status_code == 200
