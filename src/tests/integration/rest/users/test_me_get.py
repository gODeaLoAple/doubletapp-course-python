from multiprocessing.connection import Client

import pytest

from tests.integration.rest.utils import bearer, login
from tests.utils.user_mother import UserMother

API_ME = "/api/users/me/"


@pytest.mark.django_db
@pytest.mark.smoke
def test_me_when_unauthorized(client: Client):
    response = client.get(API_ME)

    assert response.status_code == 401


@pytest.mark.django_db
@pytest.mark.smoke
def test_me_when_has_not_phone(client: Client):
    password = "lool"
    user = UserMother.create_user(password=password, phone=None)
    access_token, refresh_token = login(client, user, password)
    headers = bearer(access_token)

    response = client.get(API_ME, **headers)

    assert response.status_code == 400


@pytest.mark.django_db
@pytest.mark.smoke
def test_me_works(client: Client):
    password = "lool"
    user = UserMother.create_user(password=password)
    access_token, refresh_token = login(client, user, password)
    headers = bearer(access_token)

    response = client.get(API_ME, **headers)

    assert response.status_code == 200
