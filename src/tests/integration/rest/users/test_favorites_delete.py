import pytest
from django.test.client import Client

from tests.integration.rest.users.utils import request_put_favorites
from tests.integration.rest.utils import bearer, create_authorized_user
from tests.utils.user_mother import UserMother

URL_FAVORITE = "/api/users/favorites/"


@pytest.mark.django_db
@pytest.mark.smoke
def test_favorites_delete_when_unauthorized(client: Client):
    data = {"username": "username"}

    response = request_delete_favorites(client, data)

    assert response.status_code == 401


@pytest.mark.django_db
@pytest.mark.smoke
def test_favorites_delete_when_has_not_phone(client: Client):
    user, access_token = create_authorized_user(client, phone=None)
    data = {"username": "username"}

    response = request_delete_favorites(client, data, **bearer(access_token))

    assert response.status_code == 400


@pytest.mark.django_db
@pytest.mark.smoke
def test_favorites_delete_when_incorrect_schema(client: Client):
    user, access_token = create_authorized_user(client)
    data = {}

    response = request_delete_favorites(client, data, **bearer(access_token))

    assert response.status_code == 422


@pytest.mark.django_db
@pytest.mark.smoke
def test_favorites_delete_when_user_not_found(client: Client):
    user, access_token = create_authorized_user(client)
    data = {"username": "username"}

    response = request_delete_favorites(client, data, **bearer(access_token))

    assert response.status_code == 400


@pytest.mark.django_db
@pytest.mark.smoke
def test_favorites_delete_when_not_favorite(client: Client):
    user, access_token = create_authorized_user(client)
    target = UserMother.create_other_user(user)
    data = {"username": target.username}

    response = request_delete_favorites(client, data, **bearer(access_token))

    assert response.status_code == 400


@pytest.mark.django_db
@pytest.mark.smoke
def test_favorites_delete_works(client: Client):
    user, access_token = create_authorized_user(client)
    target = UserMother.create_other_user(user)
    data = {"username": target.username}

    put_response = request_put_favorites(client, data, **bearer(access_token))
    response = request_delete_favorites(client, data, **bearer(access_token))

    assert put_response.status_code == 200
    assert response.status_code == 200


def request_delete_favorites(client: Client, data, **kwargs):
    return client.delete(URL_FAVORITE, data, **kwargs, content_type="application/json")
