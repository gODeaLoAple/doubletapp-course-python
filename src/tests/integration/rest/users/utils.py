from django.test.client import Client

URL_FAVORITE = "/api/users/favorites/"


def request_put_favorites(client: Client, data, **kwargs):
    return client.put(URL_FAVORITE, data, **kwargs, content_type="application/json")
