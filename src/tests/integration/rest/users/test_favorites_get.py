import json

import pytest
from django.test.client import Client

from tests.integration.rest.users.utils import URL_FAVORITE, request_put_favorites
from tests.integration.rest.utils import bearer, create_authorized_user
from tests.utils.user_mother import UserMother


@pytest.mark.django_db
@pytest.mark.smoke
def test_favorites_get_when_unauthorized(client: Client):
    response = request_get_favorites(client)

    assert response.status_code == 401


@pytest.mark.django_db
@pytest.mark.smoke
def test_favorites_get_when_has_not_phone(client: Client):
    user, access_token = create_authorized_user(client, phone=None)

    response = request_get_favorites(client, **bearer(access_token))

    assert response.status_code == 400


@pytest.mark.django_db
@pytest.mark.smoke
def test_favorites_get_works_when_several_users(client: Client):
    user, access_token = create_authorized_user(client)
    target1 = UserMother.create_other_user(user)
    target2 = UserMother.create_other_user(user, target1)

    request_put_favorites(client, {"username": target1.username}, **bearer(access_token))
    request_put_favorites(client, {"username": target2.username}, **bearer(access_token))
    response = request_get_favorites(client, **bearer(access_token))

    assert json.loads(response.content.decode("utf8")) == {"favorites": [target1.username, target2.username]}
    assert response.status_code == 200


@pytest.mark.django_db
@pytest.mark.smoke
def test_favorites_get_works(client: Client):
    user, access_token = create_authorized_user(client)

    response = request_get_favorites(client, **bearer(access_token))

    assert response.status_code == 200


def request_get_favorites(client: Client, **kwargs):
    return client.get(URL_FAVORITE, **kwargs)
