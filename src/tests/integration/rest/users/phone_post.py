import pytest
from django.test.client import Client

from tests.integration.rest.users.utils import request_put_favorites
from tests.integration.rest.utils import bearer, create_authorized_user
from tests.utils.user_mother import UserMother


@pytest.mark.django_db
@pytest.mark.smoke
def test_phone_post_when_unauthorized(client: Client):
    data = {"phone": 908234032}

    response = request_post_phone(client, data)

    assert response.status_code == 401


@pytest.mark.django_db
@pytest.mark.smoke
def test_phone_post_when_incorrect_schema(client: Client):
    user, access_token = create_authorized_user(client)
    data = {}

    response = request_post_phone(client, data, **bearer(access_token))

    assert response.status_code == 422


@pytest.mark.django_db
@pytest.mark.smoke
def test_phone_post_when_has_not_phone(client: Client):
    user, access_token = create_authorized_user(client, phone=None)
    data = {"phone": 908234032}

    response = request_post_phone(client, data, **bearer(access_token))

    assert response.status_code == 200


@pytest.mark.django_db
@pytest.mark.smoke
def test_phone_post_works(client: Client):
    user, access_token = create_authorized_user(client)
    target = UserMother.create_other_user(user)
    data = {"username": target.username}

    response = request_put_favorites(client, data, **bearer(access_token))

    assert response.status_code == 200


def request_post_phone(client: Client, data, **kwargs):
    return client.post("/api/users/phone/", data, **kwargs, content_type="application/json")
