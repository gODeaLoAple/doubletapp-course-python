import json
from multiprocessing.connection import Client

from tests.utils.user_mother import UserMother

LOGIN_URL = "/api/auth/login/"


def login(client: Client, user, password):
    data = {"username": user.username, "password": password}
    login_response = client.post(LOGIN_URL, data, accept="application/json", content_type="application/json")
    login_content = json.loads(login_response.content.decode("utf-8"))
    return login_content["access_token"], login_content["refresh_token"]


def bearer(access_token: str):
    return {"HTTP_AUTHORIZATION": f"Bearer {access_token}"}


def create_authorized_user(client: Client, **kwargs):
    password = kwargs["password"] if "password" in kwargs else "password"
    user = UserMother.create_user(password=password, **kwargs)
    access_token, refresh_token = login(client, user, password)
    return user, access_token
