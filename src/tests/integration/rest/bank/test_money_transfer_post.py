import json

import pytest
from django.test.client import Client

from app.internal.utils.messages import ERR_HAS_NO_PERMISSION
from tests.integration.rest.users.utils import request_put_favorites
from tests.integration.rest.utils import bearer, create_authorized_user
from tests.utils.bank_account_mother import BankAccountMother
from tests.utils.user_mother import UserMother


@pytest.mark.django_db
@pytest.mark.smoke
def test_money_transfer_post_get_when_unauthorized(client: Client):
    data = {"to": "username", "money": 10}
    response = request_post_transfer_money(client, data)

    assert response.status_code == 401


@pytest.mark.django_db
@pytest.mark.smoke
def test_money_transfer_post_get_when_has_not_phone(client: Client):
    user, access_token = create_authorized_user(client, phone=None)
    data = {"to": "username", "money": 10}

    response = request_post_transfer_money(client, data, **bearer(access_token))

    assert json.loads(response.content.decode("utf8")) == {"detail": ERR_HAS_NO_PERMISSION}
    assert response.status_code == 400


@pytest.mark.django_db
@pytest.mark.smoke
def test_money_transfer_post_get_works(client: Client):
    user, access_token = create_authorized_user(client)
    BankAccountMother.create_account(user, 1000)
    target = UserMother.create_other_user(user)
    BankAccountMother.create_account(target, 10)
    data = {"to": target.username, "money": 10}

    request_put_favorites(client, {"username": target.username}, **bearer(access_token))
    response = request_post_transfer_money(client, data, **bearer(access_token))

    assert response.status_code == 200


def request_post_transfer_money(client: Client, data, **kwargs):
    return client.post("/api/money/transfer/", data, **kwargs, content_type="application/json")
