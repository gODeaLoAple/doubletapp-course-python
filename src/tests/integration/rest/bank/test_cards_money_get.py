import json

import pytest
from django.test.client import Client

from tests.integration.rest.utils import bearer, create_authorized_user
from tests.utils.bank_account_mother import BankAccountMother
from tests.utils.card_mother import CardMother
from tests.utils.user_mother import UserMother


@pytest.mark.django_db
@pytest.mark.smoke
def test_cards_money_get_when_unauthorized(client: Client):
    response = request_get_money(client, "1111222233334444")

    assert response.status_code == 401


@pytest.mark.django_db
@pytest.mark.smoke
def test_cards_money_get_when_has_not_phone(client: Client):
    user, access_token = create_authorized_user(client, phone=None)

    response = request_get_money(client, "1111222233334444", **bearer(access_token))

    assert response.status_code == 400


@pytest.mark.django_db
@pytest.mark.smoke
def test_cards_money_get_works_when_card_not_exists(client: Client):
    user, access_token = create_authorized_user(client)

    response = request_get_money(client, "1111222233334444", **bearer(access_token))

    assert response.status_code == 400


@pytest.mark.django_db
@pytest.mark.smoke
def test_cards_money_get_works_when_incorrect_card(client: Client):
    user, access_token = create_authorized_user(client)

    response = request_get_money(client, "aaaa", **bearer(access_token))

    assert response.status_code == 400


@pytest.mark.django_db
@pytest.mark.smoke
def test_cards_money_get_works_when_card_not_owned_by_user(client: Client):
    user, access_token = create_authorized_user(client)
    target1 = UserMother.create_other_user(user)
    account = BankAccountMother.create_account(target1, 1000)
    card = CardMother.create_card(account, "1111222233334444")

    response = request_get_money(client, card.name, **bearer(access_token))

    assert response.status_code == 400


@pytest.mark.django_db
@pytest.mark.smoke
def test_cards_money_get_works(client: Client):
    user, access_token = create_authorized_user(client)
    account = BankAccountMother.create_account(user, 1000)
    card = CardMother.create_card(account, "1111222233334444")

    response = request_get_money(client, card.name, **bearer(access_token))

    assert json.loads(response.content.decode("utf8")) == {"balance": account.money}
    assert response.status_code == 200


def request_get_money(client: Client, code, **kwargs):
    return client.get(f"/api/cards/money/{code}", **kwargs)
