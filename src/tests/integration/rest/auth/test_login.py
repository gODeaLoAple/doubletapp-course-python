import json
from multiprocessing.connection import Client

import pytest

from tests.utils.user_mother import UserMother

AUTH_URL = "/api/auth/login/"


@pytest.mark.django_db
@pytest.mark.integration
def test_login_works(client: Client):
    password = "pswrd"
    user = UserMother.create_user(password=password)
    data = {"username": user.username, "password": password}

    response = client.post(AUTH_URL, data, content_type="application/json")
    content = json.loads(response.content.decode("utf-8"))

    assert response.status_code == 200
    assert "refresh_token" in content
    assert "access_token" in content


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.parametrize(
    "data",
    [
        {},
        {"username": "user"},
        {"password": "user"},
    ],
)
def test_login_when_incorrect_model(client: Client, data):
    response = client.post(AUTH_URL, data, content_type="application/json")

    assert response.status_code == 422


@pytest.mark.django_db
@pytest.mark.integration
def test_login_when_not_created(client: Client):
    data = {"username": "user", "password": "pwd"}

    response = client.post(AUTH_URL, data, content_type="application/json")

    assert response.status_code == 401
