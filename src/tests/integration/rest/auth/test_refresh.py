import datetime
import json
from multiprocessing.connection import Client

import pytest
from django.utils import timezone
from freezegun import freeze_time

from config import settings
from tests.integration.rest.utils import login
from tests.utils.user_mother import UserMother

REFRESH_URL = "/api/auth/refresh/"


@pytest.mark.django_db
@pytest.mark.integration
def test_refresh_works(client: Client):
    password = "pwd"
    user = UserMother.create_user(password=password)

    access_token, refresh_token = login(client, user, password)
    response = client.post(REFRESH_URL, {"refresh_token": refresh_token}, content_type="application/json")
    refresh_content = json.loads(response.content.decode("utf-8"))

    assert response.status_code == 200
    assert "refresh_token" in refresh_content
    assert "access_token" in refresh_content


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.parametrize("data", [{}, {"TOKEN": ""}])
def test_refresh_when_incorrect_data(client: Client, data):
    response = client.post(REFRESH_URL, data, content_type="application/json")

    assert response.status_code == 422


@pytest.mark.django_db
@pytest.mark.integration
def test_refresh_when_old_refresh_token(client: Client):
    password = "pwd"
    user = UserMother.create_user(password=password)

    access_token, refresh_token = login(client, user, password)
    login(client, user, password)
    response = client.post(REFRESH_URL, {"refresh_token": refresh_token}, content_type="application/json")

    assert response.status_code == 400


@pytest.mark.django_db
@pytest.mark.integration
def test_refresh_when_refresh_token_expired(client: Client):
    with freeze_time(datetime.datetime.now()) as freezer:
        password = "pwd"
        user = UserMother.create_user(password=password)

        access_token, refresh_token = login(client, user, password)
        delay = datetime.timedelta(seconds=settings.JWT_REFRESH_TOKEN_EXPIRED) + timezone.timedelta(seconds=10)
        freezer.tick(delta=delay)
        response = client.post(REFRESH_URL, {"refresh_token": refresh_token}, content_type="application/json")

        assert response.status_code == 400
