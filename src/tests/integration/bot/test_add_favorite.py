import pytest

from app.internal.bot.commands import ADD_FAVORITE, START
from app.internal.bot.handlers.add_favorite import add_favorite
from app.internal.bot.handlers.start import start
from app.internal.utils.messages import (
    ERR_FAVORITE_ALREADY_ADDED,
    ERR_FAVORITE_NOT_FOUND,
    ERR_HAS_NO_PERMISSION,
    ERR_SELF_FAVORITE,
    ERR_USER_NOT_EXIST,
    ERR_WRONG_COUNT_ARGUMENTS,
    INFO_ADD_FAVORITE_SUCCESS,
)
from tests.integration.bot.utils import assert_reply_message, fake_context, fake_update, fake_user, register_user
from tests.utils.user_mother import UserMother


@pytest.mark.django_db
@pytest.mark.integration
def test_add_favorite_when_not_started():
    user = fake_user()
    update = fake_update(user)

    add_favorite(update, fake_context("user"))

    assert_reply_message(update, ERR_USER_NOT_EXIST.format(START.hint))


@pytest.mark.django_db
@pytest.mark.integration
def test_add_favorite_when_not_set_phone():
    user = fake_user()
    update = fake_update(user)

    start(fake_update(user), fake_context("123"))
    add_favorite(update, fake_context("user"))

    assert_reply_message(update, ERR_HAS_NO_PERMISSION)


@pytest.mark.django_db
@pytest.mark.integration
def test_add_favorite_when_favorite_not_found():
    user = fake_user()
    update = fake_update(user)

    register_user(user)
    add_favorite(update, fake_context("user"))

    assert_reply_message(update, ERR_FAVORITE_NOT_FOUND)


@pytest.mark.django_db
@pytest.mark.integration
def test_add_favorite_when_favorite_already_added():
    user = fake_user()
    update = fake_update(user)
    other_user_entity = UserMother.create_user(telegram_id=2, username="user")

    register_user(user)
    add_favorite(fake_update(user), fake_context(other_user_entity.username))
    add_favorite(update, fake_context(other_user_entity.username))

    assert_reply_message(update, ERR_FAVORITE_ALREADY_ADDED)


@pytest.mark.django_db
@pytest.mark.integration
def test_add_favorite_when_add_self():
    user = fake_user()
    update = fake_update(user)

    user_entity = register_user(user)
    add_favorite(fake_update(user), fake_context(user_entity.username))
    add_favorite(update, fake_context(user_entity.username))

    assert_reply_message(update, ERR_SELF_FAVORITE)


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.parametrize("count", [0, 2, 10])
def test_add_favorite_when_wrong_arguments_count(count):
    user = fake_user()
    update = fake_update(user)
    args = ["0" for _ in range(count)]

    register_user(user)
    add_favorite(update, fake_context(*args))

    assert_reply_message(update, ERR_WRONG_COUNT_ARGUMENTS.format(ADD_FAVORITE.hint))


@pytest.mark.django_db
@pytest.mark.integration
def test_add_favorite():
    user = fake_user()
    update = fake_update(user)
    other_user_entity = UserMother.create_user(telegram_id=2)

    register_user(user)
    add_favorite(update, fake_context(other_user_entity.username))

    assert_reply_message(update, INFO_ADD_FAVORITE_SUCCESS)
