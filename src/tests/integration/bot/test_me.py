import pytest

from app.internal.bot.commands import START
from app.internal.bot.handlers.me import me
from app.internal.bot.handlers.start import start
from app.internal.utils.messages import (
    ERR_HAS_NO_PERMISSION,
    ERR_USER_NOT_EXIST,
    INFO_PERSONAL_FIRST_NAME,
    INFO_PERSONAL_ID,
    INFO_PERSONAL_LAST_NAME,
    INFO_PERSONAL_PHONE,
    INFO_PERSONAL_TELEGRAM_ID,
    INFO_PERSONAL_USERNAME,
)
from tests.integration.bot.utils import fake_context, fake_update, fake_user, register_user


@pytest.mark.django_db
@pytest.mark.integration
def test_me_when_not_started():
    user = fake_user()
    update = fake_update(user)

    me(update, fake_context())

    update.message.reply_text.assert_called_with(ERR_USER_NOT_EXIST.format(START.hint))


@pytest.mark.django_db
@pytest.mark.integration
def test_me_when_not_set_phone():
    user = fake_user()
    update = fake_update(user)

    start(fake_update(user), fake_context("123"))
    me(update, fake_context())

    update.message.reply_text.assert_called_with(ERR_HAS_NO_PERMISSION)


@pytest.mark.django_db
@pytest.mark.integration
def test_me():
    user = fake_user(id=1, first_name="name", last_name="surname", username="username")
    update = fake_update(user)

    user_entity = register_user(user)
    me(update, fake_context())

    update.message.reply_text.assert_called_with(
        f"""*{INFO_PERSONAL_ID}*: {user_entity.id}
*{INFO_PERSONAL_TELEGRAM_ID}*: 1
*{INFO_PERSONAL_FIRST_NAME}*: name
*{INFO_PERSONAL_LAST_NAME}*: surname
*{INFO_PERSONAL_USERNAME}*: @username
*{INFO_PERSONAL_PHONE}*: 89991112233
""",
        parse_mode="MarkdownV2",
    )
