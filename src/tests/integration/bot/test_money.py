import pytest

from app.internal.bot.commands import MONEY, START
from app.internal.bot.handlers.money import money
from app.internal.bot.handlers.start import start
from app.internal.utils.format import MarkdownV2, format_money
from app.internal.utils.messages import (
    ERR_CARD_NOT_FOUND,
    ERR_HAS_NO_PERMISSION,
    ERR_USER_NOT_EXIST,
    ERR_WRONG_COUNT_ARGUMENTS,
    INFO_PERSON_CARD_BALANCE,
)
from tests.integration.bot.utils import assert_reply_message, fake_context, fake_update, fake_user, register_user
from tests.utils.bank_account_mother import BankAccountMother
from tests.utils.card_mother import CardMother
from tests.utils.user_mother import UserMother


@pytest.mark.django_db
@pytest.mark.integration
def test_money_when_not_started():
    user = fake_user()
    update = fake_update(user)

    money(update, fake_context("0000000000000000"))

    assert_reply_message(update, ERR_USER_NOT_EXIST.format(START.hint))


@pytest.mark.django_db
@pytest.mark.integration
def test_money_when_not_set_phone():
    user = fake_user()
    update = fake_update(user)

    start(fake_update(user), fake_context("123"))
    money(update, fake_context("0000000000000000"))

    assert_reply_message(update, ERR_HAS_NO_PERMISSION)


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.parametrize("count", [0, 2, 10])
def test_money_when_wrong_arguments_count(count):
    user = fake_user()
    update = fake_update(user)
    args = ["0" for _ in range(count)]

    register_user(user)
    money(update, fake_context(*args))

    assert_reply_message(update, ERR_WRONG_COUNT_ARGUMENTS.format(MONEY.hint))


@pytest.mark.django_db
@pytest.mark.integration
def test_money_when_has_not_bank_account():
    user = fake_user()
    update = fake_update(user)

    register_user(user)
    money(update, fake_context("0000000000000000"))

    assert_reply_message(update, ERR_CARD_NOT_FOUND)


@pytest.mark.django_db
@pytest.mark.integration
def test_money_when_has_not_card():
    user = fake_user()
    update = fake_update(user)

    user_entity = register_user(user)
    BankAccountMother.create_account(user_entity, 100)
    money(update, fake_context("0000000000000000"))

    assert_reply_message(update, ERR_CARD_NOT_FOUND)


@pytest.mark.django_db
@pytest.mark.integration
def test_money_when_card_is_not_owned_by_user():
    user = fake_user(id=1)
    update = fake_update(user)
    other_user_entity = UserMother.create_user(id=2, telegram_id=2)
    other_bank_account = BankAccountMother.create_account(other_user_entity, 1000)
    other_card = CardMother.create_card(other_bank_account, "2222333344445555")

    user_entity = register_user(user)
    bank_account = BankAccountMother.create_account(user_entity, 100)
    CardMother.create_card(bank_account, "1111222233334444")
    money(update, fake_context(other_card.name))

    assert_reply_message(update, ERR_CARD_NOT_FOUND)


@pytest.mark.django_db
@pytest.mark.integration
def test_money():
    user = fake_user()
    update = fake_update(user)

    user_entity = register_user(user)
    bank_account = BankAccountMother.create_account(user_entity, 100)
    card = CardMother.create_card(bank_account, "1111222233334444")
    money(update, fake_context(card.name))

    assert_reply_message(update, f"{INFO_PERSON_CARD_BALANCE}: *{format_money(100)}*", parse_mode=MarkdownV2.MODE)
