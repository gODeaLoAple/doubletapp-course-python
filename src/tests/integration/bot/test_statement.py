import pytest

from app.internal.bot.commands import START, STATEMENT
from app.internal.bot.handlers.start import start
from app.internal.bot.handlers.statement import create_statement_text, statement
from app.internal.utils.format import MarkdownV2
from app.internal.utils.messages import (
    ERR_BANK_ACCOUNT_NOT_FOUND,
    ERR_HAS_NO_PERMISSION,
    ERR_USER_NOT_EXIST,
    ERR_WRONG_COUNT_ARGUMENTS,
)
from tests.integration.bot.utils import assert_reply_message, fake_context, fake_update, fake_user, register_user
from tests.utils.actions import add_transactions
from tests.utils.bank_account_mother import BankAccountMother
from tests.utils.card_mother import CardMother
from tests.utils.transaction_mother import TransactionMother
from tests.utils.user_mother import UserMother


@pytest.mark.django_db
@pytest.mark.integration
def test_statement_when_not_started():
    user = fake_user()
    update = fake_update(user)

    statement(update, fake_context("0"))

    assert_reply_message(update, ERR_USER_NOT_EXIST.format(START.hint))


@pytest.mark.django_db
@pytest.mark.integration
def test_statement_when_not_set_phone():
    user = fake_user()
    update = fake_update(user)

    start(fake_update(user), fake_context("123"))
    statement(update, fake_context("0"))

    assert_reply_message(update, ERR_HAS_NO_PERMISSION)


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.parametrize("count", [0, 2])
def test_statement_when_wrong_arguments_count(count):
    user = fake_user()
    update = fake_update(user)
    args = ["0" for _ in range(count)]

    register_user(user)
    statement(update, fake_context(*args))

    assert_reply_message(update, ERR_WRONG_COUNT_ARGUMENTS.format(STATEMENT.hint))


@pytest.mark.django_db
@pytest.mark.integration
def test_statement_when_bank_account_not_exist():
    user = fake_user()
    update = fake_update(user)

    register_user(user)
    statement(update, fake_context(""))

    assert_reply_message(update, ERR_BANK_ACCOUNT_NOT_FOUND)


@pytest.mark.django_db
@pytest.mark.integration
def test_statement_when_bank_account_is_not_owned_by_user():
    user = fake_user(telegram_id=2)
    other_user_entity = UserMother.create_user()
    other_bank_account = BankAccountMother.create_account(other_user_entity)
    update = fake_update(user)

    register_user(user)
    statement(update, fake_context(str(other_bank_account)))

    assert_reply_message(update, ERR_BANK_ACCOUNT_NOT_FOUND)


@pytest.mark.django_db
@pytest.mark.integration
def test_statement_when_card_is_not_owned_by_user():
    user = fake_user(telegram_id=2)
    other_user_entity = UserMother.create_user()
    other_bank_account = BankAccountMother.create_account(other_user_entity)
    other_card = CardMother.create_card(other_bank_account)
    update = fake_update(user)

    register_user(user)
    statement(update, fake_context(other_card))

    assert_reply_message(update, ERR_BANK_ACCOUNT_NOT_FOUND)


@pytest.mark.django_db
@pytest.mark.integration
def test_statement_by_account_id():
    user = fake_user(telegram_id=2)
    update = fake_update(user)

    user_entity = register_user(user)
    account = BankAccountMother.create_account(user_entity, 1000)
    cards, transactions = add_transactions(user_entity, account)
    statement(update, fake_context(account.id))

    text = _create_statement_text(user_entity.username, account.id, cards, transactions)
    assert_reply_message(update, text, parse_mode=MarkdownV2.MODE)


@pytest.mark.django_db
@pytest.mark.integration
def test_statement_by_card():
    user = fake_user(telegram_id=2)
    update = fake_update(user)

    user_entity = register_user(user)
    account = BankAccountMother.create_account(user_entity, 1000)
    cards, transactions = add_transactions(user_entity, account)
    statement(update, fake_context(cards[0]))

    text = _create_statement_text(user_entity.username, account.id, cards, transactions)
    assert_reply_message(update, text, parse_mode=MarkdownV2.MODE)


def _create_statement_text(username, account_id, cards_names, transactions):
    dto_list = reversed(sorted(map(TransactionMother.to_dto, transactions), key=lambda x: x.date))
    return create_statement_text(username, account_id, cards_names, dto_list)
