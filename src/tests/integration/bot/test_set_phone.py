import pytest

from app.internal.bot.commands import SET_PHONE, START
from app.internal.bot.handlers.set_phone import set_phone
from app.internal.bot.handlers.start import start
from app.internal.users.db.models import AuthenticatedUser
from app.internal.utils.messages import (
    ERR_NOT_CORRECT_PHONE_NUMBER,
    ERR_PHONE_ALREADY_EXISTS,
    ERR_USER_NOT_EXIST,
    ERR_WRONG_COUNT_ARGUMENTS,
    INFO_PHONE_SET,
)
from tests.integration.bot.utils import fake_context, fake_update, fake_user
from tests.utils.user_mother import UserMother


@pytest.mark.django_db
@pytest.mark.integration
def test_set_phone_when_not_started():
    user = fake_user()
    update = fake_update(user)

    set_phone(update, fake_context("89019012222"))

    assert not AuthenticatedUser.objects.filter(telegram_id=user.id).exists()
    update.message.reply_text.assert_called_with(ERR_USER_NOT_EXIST.format(START.hint))


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.parametrize("count", [0, 2])
def test_set_phone_when_wrong_arguments_count(count):
    user = fake_user()
    update = fake_update(user)
    args = ["0" for _ in range(count)]

    start(fake_update(user), fake_context("123"))
    set_phone(update, fake_context(*args))

    user_entity = AuthenticatedUser.objects.get(telegram_id=user.id)
    assert user_entity.phone is None
    update.message.reply_text.assert_called_with(ERR_WRONG_COUNT_ARGUMENTS.format(SET_PHONE.hint))


@pytest.mark.django_db
@pytest.mark.integration
def test_set_phone_when_wrong_phone():
    user = fake_user()
    update = fake_update(user)

    start(update, fake_context("123"))
    set_phone(update, fake_context("aaaa"))

    user_entity = AuthenticatedUser.objects.get(telegram_id=user.id)
    assert user_entity.phone is None
    update.message.reply_text.assert_called_with(ERR_NOT_CORRECT_PHONE_NUMBER)


@pytest.mark.django_db
@pytest.mark.integration
def test_set_phone_when_other_user_has_same_phone():
    user = fake_user(id=1)
    update = fake_update(user)
    other_user_entity = UserMother.create_user(telegram_id=2)

    start(update, fake_context("123"))
    set_phone(update, fake_context(other_user_entity.phone))

    user_entity = AuthenticatedUser.objects.get(telegram_id=user.id)
    assert user_entity.phone is None
    update.message.reply_text.assert_called_with(ERR_PHONE_ALREADY_EXISTS)


@pytest.mark.django_db
@pytest.mark.integration
def test_set_phone():
    user = fake_user()
    update = fake_update(user)
    phone_number = "89019201234"

    start(update, fake_context("123"))
    set_phone(update, fake_context(phone_number))

    user_entity = AuthenticatedUser.objects.get(telegram_id=user.id)
    assert user_entity.phone == phone_number
    update.message.reply_text.assert_called_with(INFO_PHONE_SET)


@pytest.mark.django_db
@pytest.mark.integration
def test_set_phone_when_several_times():
    user = fake_user()
    update = fake_update(user)
    phone_number = "89019201234"
    other_phone_number = "89019201235"

    start(fake_update(user), fake_context("123"))
    set_phone(fake_update(user), fake_context(phone_number))
    set_phone(update, fake_context(other_phone_number))

    user_entity = AuthenticatedUser.objects.get(telegram_id=user.id)
    assert user_entity.phone == other_phone_number
    update.message.reply_text.assert_called_with(INFO_PHONE_SET)
