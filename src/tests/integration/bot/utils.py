from unittest.mock import MagicMock, PropertyMock

from telegram.ext import CallbackContext

from app.internal.bot.handlers.set_phone import set_phone
from app.internal.bot.handlers.start import start
from app.internal.users.db.models import AuthenticatedUser


def fake_user(**kwargs):
    user = MagicMock()
    kwargs = dict(id=1, first_name="first_name", last_name="last_name", username="username", is_bot=False) | kwargs
    user.configure_mock(**kwargs)
    return user


def fake_update(user=None) -> MagicMock:
    user = user or fake_user()

    message = MagicMock()
    type(message).reply_text = MagicMock()

    update_mock = MagicMock()
    type(update_mock).message = message
    type(update_mock).effective_user = user
    return update_mock


def fake_context(*args) -> CallbackContext:
    context = MagicMock()
    type(context).args = PropertyMock(return_value=list(map(str, args)))
    return context


def register_user(user):
    start(fake_update(user), fake_context("123"))
    set_phone(fake_update(user), fake_context("89991112233"))
    return AuthenticatedUser.objects.get(telegram_id=user.id)


def assert_reply_message(update, text, **kwargs):
    update.message.reply_text.assert_called_once_with(text, **kwargs)
