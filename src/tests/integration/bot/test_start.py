import pytest

from app.internal.bot.commands import START
from app.internal.bot.handlers.start import start
from app.internal.users.db.models import AuthenticatedUser
from app.internal.utils.messages import ERR_PASSWORD_REQUIRED, ERR_WORD, INFO_USER_SAVED, INFO_USER_UPDATED
from tests.integration.bot.utils import assert_reply_message, fake_context, fake_update, fake_user


@pytest.mark.django_db
@pytest.mark.integration
def test_start_when_first_time():
    user = fake_user()
    update = fake_update(user)

    start(update, fake_context("password"))

    user_entity = AuthenticatedUser.objects.get(telegram_id=user.id)
    assert user_entity.first_name == user.first_name
    assert user_entity.last_name == user.last_name
    assert user_entity.username == user.username
    assert user_entity.phone is None
    update.message.reply_text.assert_called_with(INFO_USER_SAVED)


@pytest.mark.django_db
@pytest.mark.integration
def test_start_when_not_authenticated():
    user = fake_user()
    update = fake_update(user)

    start(update, fake_context())

    assert_reply_message(update, ERR_PASSWORD_REQUIRED.format(START.hint))


@pytest.mark.django_db
@pytest.mark.integration
def test_start_when_incorrect_data():
    user = fake_user()
    user.username = None
    update = fake_update(user)

    start(update, fake_context("password"))

    assert_reply_message(update, ERR_WORD)


@pytest.mark.django_db
@pytest.mark.integration
def test_start_when_several_times():
    last_name = "last_name"
    user = fake_user(last_name=None)
    update = fake_update(user)

    start(update, fake_context("pass"))
    user.last_name = last_name
    start(update, fake_context())

    user_entity = AuthenticatedUser.objects.get(telegram_id=user.id)
    assert user_entity.last_name == last_name
    update.message.reply_text.assert_called_with(INFO_USER_UPDATED)
