import pytest

from app.internal.bank.db.models import BankAccount
from app.internal.bot.commands import START
from app.internal.bot.handlers.add_favorite import add_favorite
from app.internal.bot.handlers.start import start
from app.internal.bot.handlers.transfer_money import transfer_money
from app.internal.utils.messages import (
    ERR_BANK_ACCOUNT_NOT_FOUND,
    ERR_HAS_NO_PERMISSION,
    ERR_NEGATIVE_MONEY,
    ERR_NOT_ENOUGH_MONEY,
    ERR_NOT_FAVORITE,
    ERR_NOT_MONEY_WHEN_TRANSFER_MONEY,
    ERR_TARGET_HAS_MANY_BANK_ACCOUNTS_WITH_HINT,
    ERR_USER_BANK_ACCOUNT_NOT_FOUND,
    ERR_USER_HAS_MANY_BANK_ACCOUNTS_WITH_HINT,
    ERR_USER_HAS_NOT_BANK_ACCOUNT,
    ERR_USER_NOT_EXIST,
    INFO_TRANSFER_MONEY_SUCCESS,
)
from tests.integration.bot.utils import assert_reply_message, fake_context, fake_update, fake_user, register_user
from tests.utils.bank_account_mother import BankAccountMother
from tests.utils.card_mother import CardMother
from tests.utils.user_mother import UserMother


@pytest.mark.django_db
@pytest.mark.integration
def test_transfer_money_when_not_started():
    user = fake_user()
    update = fake_update(user)

    transfer_money(update, fake_context("user", 10))

    assert_reply_message(update, ERR_USER_NOT_EXIST.format(START.hint))


@pytest.mark.django_db
@pytest.mark.integration
def test_transfer_money_when_not_set_phone():
    user = fake_user()
    update = fake_update(user)

    start(fake_update(user), fake_context("123"))
    transfer_money(update, fake_context("user", 10))

    assert_reply_message(update, ERR_HAS_NO_PERMISSION)


@pytest.mark.django_db
@pytest.mark.integration
def test_transfer_money_when_target_user_not_exist():
    user = fake_user()
    update = fake_update(user)

    register_user(user)
    transfer_money(update, fake_context("user", 10))

    assert_reply_message(update, ERR_BANK_ACCOUNT_NOT_FOUND)


@pytest.mark.django_db
@pytest.mark.integration
def test_transfer_money_when_target_user_has_no_bank_account():
    user = fake_user()
    other_user_entity = UserMother.create_user()
    update = fake_update(user)

    register_user(user)
    transfer_money(update, fake_context(other_user_entity.username, 10))

    assert_reply_message(update, ERR_BANK_ACCOUNT_NOT_FOUND)


@pytest.mark.django_db
@pytest.mark.integration
def test_transfer_money_when_target_user_not_favorite():
    user = fake_user()
    update = fake_update(user)
    other_user_entity = UserMother.create_user(telegram_id=15)
    BankAccountMother.create_account(other_user_entity)
    register_user(user)

    transfer_money(update, fake_context(other_user_entity.username, 10))

    assert_reply_message(update, ERR_NOT_FAVORITE)


@pytest.mark.django_db
@pytest.mark.integration
def test_transfer_money_by_username_when_target_has_many_bank_accounts():
    user = fake_user()
    update = fake_update(user)
    other_user_entity = UserMother.create_user(telegram_id=15)
    BankAccountMother.create_account(other_user_entity)
    BankAccountMother.create_account(other_user_entity)
    BankAccountMother.create_account(other_user_entity)

    register_user(user)
    add_favorite(fake_update(user), fake_context(other_user_entity.username))
    transfer_money(update, fake_context(other_user_entity.username, 10))

    assert_reply_message(update, ERR_TARGET_HAS_MANY_BANK_ACCOUNTS_WITH_HINT)


@pytest.mark.django_db
@pytest.mark.integration
def test_transfer_money_when_user_has_no_bank_accounts():
    user = fake_user()
    update = fake_update(user)
    other_user_entity = UserMother.create_user(telegram_id=15)
    BankAccountMother.create_account(other_user_entity)

    register_user(user)
    add_favorite(fake_update(user), fake_context(other_user_entity.username))
    transfer_money(update, fake_context(other_user_entity.username, 10))

    assert_reply_message(update, ERR_USER_HAS_NOT_BANK_ACCOUNT)


@pytest.mark.django_db
@pytest.mark.integration
def test_transfer_money_when_user_has_many_bank_accounts():
    user = fake_user()
    update = fake_update(user)
    other_user_entity = UserMother.create_user(telegram_id=15)
    BankAccountMother.create_account(other_user_entity)

    user_entity = register_user(user)
    BankAccountMother.create_account(user_entity)
    BankAccountMother.create_account(user_entity)
    BankAccountMother.create_account(user_entity)
    add_favorite(fake_update(user), fake_context(other_user_entity.username))
    transfer_money(update, fake_context(other_user_entity.username, 10))

    assert_reply_message(update, ERR_USER_HAS_MANY_BANK_ACCOUNTS_WITH_HINT)


@pytest.mark.django_db
@pytest.mark.integration
def test_transfer_money_when_specified_bank_account_id_is_not_his():
    money = 100
    user = fake_user()
    user_entity, bank_account, other_user_entity, other_bank_account = create_success_case(user)
    update = fake_update(user)

    transfer_money(update, fake_context(other_user_entity.username, money, other_bank_account.id))

    assert_reply_message(update, ERR_USER_BANK_ACCOUNT_NOT_FOUND)


@pytest.mark.django_db
@pytest.mark.integration
def test_transfer_money_when_specified_bank_account_id_is_not_exists():
    money = 100
    user = fake_user()
    user_entity, bank_account, other_user_entity, other_bank_account = create_success_case(user)
    update = fake_update(user)

    transfer_money(update, fake_context(other_user_entity.username, money, -1))

    assert_reply_message(update, ERR_USER_BANK_ACCOUNT_NOT_FOUND)


@pytest.mark.django_db
@pytest.mark.integration
def test_transfer_money_when_specified_card_number_is_not_his():
    money = 100
    user = fake_user()
    user_entity, bank_account, other_user_entity, other_bank_account = create_success_case(user)
    update = fake_update(user)
    other_card = CardMother.create_card(other_bank_account)

    transfer_money(update, fake_context(other_user_entity.username, money, other_card.name))

    assert_reply_message(update, ERR_USER_BANK_ACCOUNT_NOT_FOUND)


@pytest.mark.django_db
@pytest.mark.integration
def test_transfer_money_when_user_has_not_enough_money():
    user = fake_user()
    update = fake_update(user)
    other_user_entity = UserMother.create_user(telegram_id=15)
    BankAccountMother.create_account(other_user_entity)

    user_entity = register_user(user)
    BankAccountMother.create_account(user_entity, 1000)
    add_favorite(fake_update(user), fake_context(other_user_entity.username))
    transfer_money(update, fake_context(other_user_entity.username, 10000))

    assert_reply_message(update, ERR_NOT_ENOUGH_MONEY)


@pytest.mark.django_db
@pytest.mark.integration
def test_transfer_money_when_not_money():
    user = fake_user()
    user_entity, bank_account, other_user_entity, other_bank_account = create_success_case(user)
    update = fake_update(user)

    transfer_money(update, fake_context(other_user_entity.username, "aaa"))

    assert_reply_message(update, ERR_NOT_MONEY_WHEN_TRANSFER_MONEY)


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.parametrize("money", [-100, 0])
def test_transfer_money_when_incorrect_money(money):
    user = fake_user()
    user_entity, bank_account, other_user_entity, other_bank_account = create_success_case(user)
    update = fake_update(user)

    transfer_money(update, fake_context(other_user_entity.username, money))

    assert_reply_message(update, ERR_NEGATIVE_MONEY)


@pytest.mark.django_db
@pytest.mark.integration
def test_transfer_money_by_username():
    money = 100
    user = fake_user()
    user_entity, bank_account, other_user_entity, other_bank_account = create_success_case(user)
    update = fake_update(user)

    transfer_money(update, fake_context(other_user_entity.username, money))

    assert_reply_message(update, INFO_TRANSFER_MONEY_SUCCESS)
    assert_transfer(bank_account, other_bank_account, money)


@pytest.mark.django_db
@pytest.mark.integration
def test_transfer_money_by_bank_account():
    money = 100
    user = fake_user()
    user_entity, bank_account, other_user_entity, other_bank_account = create_success_case(user)
    update = fake_update(user)

    transfer_money(update, fake_context(other_bank_account.id, money))

    assert_reply_message(update, INFO_TRANSFER_MONEY_SUCCESS)
    assert_transfer(bank_account, other_bank_account, money)


@pytest.mark.django_db
@pytest.mark.integration
def test_transfer_money_by_bank_account_when_target_has_many_bank_accounts():
    money = 100
    user = fake_user()
    user_entity, bank_account, other_user_entity, other_bank_account = create_success_case(user)
    update = fake_update(user)
    other_bank_accounts = [
        BankAccountMother.create_account(other_user_entity, 1000),
        BankAccountMother.create_account(other_user_entity, 1000),
    ]

    transfer_money(update, fake_context(other_bank_account.id, money))

    assert_reply_message(update, INFO_TRANSFER_MONEY_SUCCESS)
    assert_transfer(bank_account, other_bank_account, money)
    assert other_bank_accounts[0].money == 1000
    assert other_bank_accounts[1].money == 1000


@pytest.mark.django_db
@pytest.mark.integration
def test_transfer_money_by_card():
    money = 100
    user = fake_user()
    user_entity, bank_account, other_user_entity, other_bank_account = create_success_case(user)
    other_card = CardMother.create_card(other_bank_account)
    update = fake_update(user)

    transfer_money(update, fake_context(other_card.name, money))

    assert_reply_message(update, INFO_TRANSFER_MONEY_SUCCESS)
    assert_transfer(bank_account, other_bank_account, money)


@pytest.mark.django_db
@pytest.mark.integration
def test_transfer_money_by_card_when_target_has_many_bank_accounts():
    money = 100
    user = fake_user()
    user_entity, bank_account, other_user_entity, other_bank_account = create_success_case(user)
    update = fake_update(user)
    other_bank_accounts = [
        BankAccountMother.create_account(other_user_entity, 1000),
        BankAccountMother.create_account(other_user_entity, 1000),
    ]
    other_card = CardMother.create_card(other_bank_account, 1000)

    transfer_money(update, fake_context(other_card.name, money))

    assert_reply_message(update, INFO_TRANSFER_MONEY_SUCCESS)
    assert_transfer(bank_account, other_bank_account, money)
    assert other_bank_accounts[0].money == 1000
    assert other_bank_accounts[1].money == 1000


@pytest.mark.django_db
@pytest.mark.integration
def test_transfer_money_when_user_has_many_bank_accounts_but_specify_card():
    money = 100
    user = fake_user()
    user_entity, bank_account, other_user_entity, other_bank_account = create_success_case(user)
    update = fake_update(user)
    bank_accounts = [BankAccountMother.create_account(user_entity), BankAccountMother.create_account(user_entity)]
    card = CardMother.create_card(bank_account)

    transfer_money(update, fake_context(other_user_entity.username, money, card.name))

    assert_reply_message(update, INFO_TRANSFER_MONEY_SUCCESS)
    assert_transfer(bank_account, other_bank_account, money)
    assert bank_accounts[0].money == 1000
    assert bank_accounts[1].money == 1000


@pytest.mark.django_db
@pytest.mark.integration
def test_transfer_money_when_user_has_many_bank_accounts_but_specify_bank_account_id():
    money = 100
    user = fake_user()
    user_entity, bank_account, other_user_entity, other_bank_account = create_success_case(user)
    update = fake_update(user)
    bank_accounts = [BankAccountMother.create_account(user_entity), BankAccountMother.create_account(user_entity)]

    transfer_money(update, fake_context(other_user_entity.username, money, bank_account.id))

    assert_reply_message(update, INFO_TRANSFER_MONEY_SUCCESS)
    assert_transfer(bank_account, other_bank_account, money)
    assert bank_accounts[0].money == 1000
    assert bank_accounts[1].money == 1000


def assert_transfer(bank_account, other_bank_account, money):
    assert BankAccount.objects.get(id=bank_account.id).money == bank_account.money - money
    assert BankAccount.objects.get(id=other_bank_account.id).money == other_bank_account.money + money


def create_success_case(user):
    other_user_entity = UserMother.create_user(telegram_id=15)
    other_bank_account = BankAccountMother.create_account(other_user_entity)

    user_entity = register_user(user)
    bank_account = BankAccountMother.create_account(user_entity, 1000)
    add_favorite(fake_update(user), fake_context(other_user_entity.username))
    return user_entity, bank_account, other_user_entity, other_bank_account
