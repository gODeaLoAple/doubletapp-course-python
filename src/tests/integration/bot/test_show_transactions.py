import pytest

from app.internal.bot.commands import SHOW_TRANSACTIONS, START
from app.internal.bot.handlers.show_transactions import create_transactions_text, show_transactions
from app.internal.bot.handlers.start import start
from app.internal.utils.messages import (
    ERR_HAS_NO_PERMISSION,
    ERR_USER_NOT_EXIST,
    ERR_WRONG_COUNT_ARGUMENTS,
    INFO_TRANSACTION_HISTORY_EMPTY,
)
from tests.integration.bot.utils import assert_reply_message, fake_context, fake_update, fake_user, register_user
from tests.utils.actions import add_transactions
from tests.utils.bank_account_mother import BankAccountMother
from tests.utils.user_mother import UserMother


@pytest.mark.django_db
@pytest.mark.integration
def test_show_transactions_when_not_started():
    user = fake_user()
    update = fake_update(user)

    show_transactions(update, fake_context())

    assert_reply_message(update, ERR_USER_NOT_EXIST.format(START.hint))


@pytest.mark.django_db
@pytest.mark.integration
def test_statement_when_not_set_phone():
    user = fake_user()
    update = fake_update(user)

    start(fake_update(user), fake_context("123"))
    show_transactions(update, fake_context())

    assert_reply_message(update, ERR_HAS_NO_PERMISSION)


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.parametrize("count", [1, 2])
def test_statement_when_wrong_arguments_count(count):
    user = fake_user()
    update = fake_update(user)
    args = ["0" for _ in range(count)]

    register_user(user)
    show_transactions(update, fake_context(*args))

    assert_reply_message(update, ERR_WRONG_COUNT_ARGUMENTS.format(SHOW_TRANSACTIONS.hint))


@pytest.mark.django_db
@pytest.mark.integration
def test_statement_when_bank_account_not_exist():
    user = fake_user()
    update = fake_update(user)

    register_user(user)
    show_transactions(update, fake_context())

    assert_reply_message(update, INFO_TRANSACTION_HISTORY_EMPTY)


@pytest.mark.django_db
@pytest.mark.integration
def test_statement_when_has_not_transactions():
    user = fake_user()
    update = fake_update(user)

    register_user(user)
    show_transactions(update, fake_context())

    assert_reply_message(update, INFO_TRANSACTION_HISTORY_EMPTY)


@pytest.mark.django_db
@pytest.mark.integration
def test_statement_when_has_not_transactions_but_has_bank_account():
    user = fake_user()
    update = fake_update(user)

    user_entity = register_user(user)
    BankAccountMother.create_account(user_entity)
    show_transactions(update, fake_context())

    assert_reply_message(update, INFO_TRANSACTION_HISTORY_EMPTY)


@pytest.mark.django_db
@pytest.mark.integration
def test_statement_when_has_transactions():
    user = fake_user()
    update = fake_update(user)

    user_entity = register_user(user)
    account = BankAccountMother.create_account(user_entity)
    other = UserMother.create_user(telegram_id=2)
    other_account = BankAccountMother.create_account(other)
    add_transactions(user_entity, account, other, other_account)
    show_transactions(update, fake_context())

    assert_reply_message(update, create_transactions_text([other.username]))
