import pytest

from app.internal.bot.handlers.help import handle_help
from tests.integration.bot.utils import fake_context, fake_update, fake_user


@pytest.mark.django_db
@pytest.mark.integration
def test_help_works():
    user = fake_user()
    update = fake_update(user)

    handle_help(update, fake_context())

    update.message.reply_text.assert_called_once()
