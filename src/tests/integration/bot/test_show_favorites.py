import pytest

from app.internal.bot.commands import SHOW_FAVORITES, START
from app.internal.bot.handlers.add_favorite import add_favorite
from app.internal.bot.handlers.show_favorites import show_favorites
from app.internal.bot.handlers.start import start
from app.internal.utils.messages import (
    ERR_HAS_NO_PERMISSION,
    ERR_USER_NOT_EXIST,
    ERR_WRONG_COUNT_ARGUMENTS,
    INFO_FAVORITES_LIST,
    INFO_FAVORITES_LIST_EMPTY,
)
from tests.integration.bot.utils import assert_reply_message, fake_context, fake_update, fake_user, register_user
from tests.utils.user_mother import UserMother


@pytest.mark.django_db
@pytest.mark.integration
def test_show_favorites_when_not_started():
    user = fake_user()
    update = fake_update(user)

    show_favorites(update, fake_context())

    assert_reply_message(update, ERR_USER_NOT_EXIST.format(START.hint))


@pytest.mark.django_db
@pytest.mark.integration
def test_show_favorites_when_not_set_phone():
    user = fake_user()
    update = fake_update(user)

    start(fake_update(user), fake_context("123"))
    show_favorites(update, fake_context())

    assert_reply_message(update, ERR_HAS_NO_PERMISSION)


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.parametrize("count", [1, 10])
def test_show_favorites_when_wrong_arguments_count(count):
    user = fake_user()
    update = fake_update(user)
    args = ["0" for _ in range(count)]

    register_user(user)
    show_favorites(update, fake_context(*args))

    assert_reply_message(update, ERR_WRONG_COUNT_ARGUMENTS.format(SHOW_FAVORITES.hint))


@pytest.mark.django_db
@pytest.mark.integration
def test_show_favorites_when_has_no_favorites():
    user = fake_user()
    update = fake_update(user)

    register_user(user)
    show_favorites(update, fake_context())

    assert_reply_message(update, INFO_FAVORITES_LIST_EMPTY)


@pytest.mark.django_db
@pytest.mark.integration
def test_show_favorites_when_has_several_favorites():
    user = fake_user(id=100)
    update = fake_update(user)
    users_entities = list(UserMother.generate_users())[:3]

    register_user(user)
    for u in users_entities:
        add_favorite(fake_update(user), fake_context(u.username))
    show_favorites(update, fake_context())

    assert_reply_message(
        update,
        f"""{INFO_FAVORITES_LIST}
@{users_entities[0].username}
@{users_entities[1].username}
@{users_entities[2].username}""",
    )
