from app.internal.bank.db.models import BankAccount, Card


class CardMother:
    @staticmethod
    def create_card(bank_account: BankAccount, name="0000000000000000"):
        return Card.objects.create(bank_account=bank_account, name=name)
