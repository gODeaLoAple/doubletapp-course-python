from decimal import Decimal

from django.utils import timezone

from app.internal.bot.services.favorite_service import add_favorite_by_username
from tests.utils.bank_account_mother import BankAccountMother
from tests.utils.card_mother import CardMother
from tests.utils.transaction_mother import TransactionMother
from tests.utils.user_mother import UserMother


def make_favorite(user, other_user):
    add_favorite_by_username(user.id, other_user.username)


def add_transactions(user_entity, account, other=None, other_account=None):
    cards = [
        CardMother.create_card(account, "1111222233334444"),
        CardMother.create_card(account, "1111222233334445"),
        CardMother.create_card(account, "1111222233334447"),
    ]
    other = other or UserMother.create_user(telegram_id=user_entity.telegram_id + 1)
    other_account = other_account or BankAccountMother.create_account(other, 1000)
    now = timezone.now()
    transactions = [
        TransactionMother.create_transaction(account, other_account, Decimal(100.00), now),
        TransactionMother.create_transaction(account, other_account, Decimal(101.00), now + timezone.timedelta(1)),
        TransactionMother.create_transaction(account, other_account, Decimal(102.00), now + timezone.timedelta(2)),
        TransactionMother.create_transaction(other_account, account, Decimal(103.00), now + timezone.timedelta(3)),
        TransactionMother.create_transaction(other_account, account, Decimal(104.00), now + timezone.timedelta(4)),
        TransactionMother.create_transaction(other_account, account, Decimal(105.00), now + timezone.timedelta(5)),
    ]
    return [x.name for x in cards], transactions


def sort_transactions(dto_list):
    return list(sorted(dto_list, key=lambda x: (x.date, x.sender_username, x.recipient_username)))
