from app.internal.users.db.models import AuthenticatedUser

_DEFAULT_USERS = [
    dict(
        telegram_id=1,
        username="vandarkhole",
        password="123kek",
        first_name="Van",
        last_name="Darkhole",
        phone="89019011122",
    ),
    dict(
        telegram_id=2,
        username="billykiss",
        password="123kek",
        first_name="Billy",
        last_name="Harrington",
        phone="89019011123",
    ),
    dict(
        telegram_id=3,
        username="leedanny",
        password="123kek",
        first_name="Danny",
        last_name="Lee",
        phone="89019011124",
    ),
    dict(
        telegram_id=4,
        username="thepirate",
        password="123kek",
        first_name="johnny",
        last_name="priate",
        phone="89019011125",
    ),
]


class UserMother:
    @staticmethod
    def create_user(**kwargs):
        return UserMother._create_user(**(_DEFAULT_USERS[0] | kwargs))

    @staticmethod
    def create_users(count):
        return [UserMother._create_user(**u) for u in _DEFAULT_USERS[:count]]

    @staticmethod
    def generate_users():
        for u in _DEFAULT_USERS:
            yield UserMother._create_user(**u)

    @staticmethod
    def _create_user(**kwargs):
        username, telegram_id, password = kwargs["username"], kwargs["telegram_id"], kwargs["password"]
        del kwargs["username"]
        del kwargs["telegram_id"]
        del kwargs["password"]
        return AuthenticatedUser.objects.create_user(
            username=username, telegram_id=telegram_id, password=password, **kwargs
        )

    @staticmethod
    def create_other_user(*users):
        for data in _DEFAULT_USERS:
            if data["username"] not in [x.username for x in users]:
                return UserMother.create_user(**data)
