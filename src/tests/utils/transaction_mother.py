from django.utils import timezone

from app.internal.bank.db.models import Transaction
from app.internal.bank.domain.entities import TransactionSchema


class TransactionMother:
    @staticmethod
    def create_transaction(from_account, to_account, money, date=None):
        date = date or timezone.now()
        return Transaction.objects.create(
            sender_account=from_account, recipient_account=to_account, money=money, date=date
        )

    @staticmethod
    def to_dto(transaction: Transaction):
        return TransactionSchema(
            transaction.sender_account.user.username,
            transaction.recipient_account.user.username,
            transaction.money,
            transaction.date,
        )
