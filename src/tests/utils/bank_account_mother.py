from app.internal.bank.db.models import BankAccount
from app.internal.users.db.models import AuthenticatedUser


class BankAccountMother:
    @staticmethod
    def create_account(user: AuthenticatedUser, money=1000):
        return BankAccount.objects.create(user=user, money=money)
