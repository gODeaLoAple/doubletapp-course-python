from django.contrib import admin

from app.internal.auth.presentation.admin import IssuedTokenAdmin
from app.internal.bank.presentation.admin import BankAccountAdmin, CardAdmin, TransactionAdmin
from app.internal.users.presentation.admin import AuthenticatedUserAdmin, Favorite

admin.site.site_title = "Backend course"
admin.site.site_header = "Backend course"
