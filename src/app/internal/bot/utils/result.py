class CommandResult:
    def __init__(self, text, **kwargs):
        self.text = text
        self.kwargs = kwargs


class OkResult(CommandResult):
    def __init__(self, text, **kwargs):
        super().__init__(text, **kwargs)


class ErrorResult(CommandResult):
    def __init__(self, text, **kwargs):
        super().__init__(text, **kwargs)


def ok(text, **kwargs):
    return OkResult(text, **kwargs)


def error(text):
    return ErrorResult(text)
