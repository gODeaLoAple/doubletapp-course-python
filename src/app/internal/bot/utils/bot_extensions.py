def try_delete_message(update, context):
    bot = context.bot
    message = update.message
    try:
        bot.delete_message(chat_id=message.chat.id, message_id=message.message_id)
    except:
        ...
