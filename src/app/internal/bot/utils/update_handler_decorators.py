from telegram import Update
from telegram.ext import CallbackContext

from app.internal.bot.commands import HELP, START
from app.internal.bot.dto.dto_factory import DtoFactory
from app.internal.bot.dto.has_phone_dto import HasPhoneDto
from app.internal.bot.services.exceptions import HasNotPhoneError
from app.internal.bot.services.user_service import get_user_fields_by_telegram
from app.internal.bot.services.validators import validate_has_phone
from app.internal.bot.utils.bot_extensions import try_delete_message
from app.internal.bot.utils.result import error
from app.internal.users.db.models import AuthenticatedUser
from app.internal.utils.messages import ERR_HAS_NO_PERMISSION, ERR_USER_NOT_EXIST, ERR_WRONG_COUNT_ARGUMENTS


def require_dto(factory: DtoFactory):
    fields_names = factory.get_fields()

    def wrapper(handler):
        def inner_wrapper(update: Update, context: CallbackContext):
            user = update.effective_user
            try:
                user_entity = get_user_fields_by_telegram(user.id, fields_names)
            except AuthenticatedUser.DoesNotExist:
                return error(ERR_USER_NOT_EXIST.format(START.hint))
            return handler(update, context, factory.from_values(user_entity))

        return inner_wrapper

    return wrapper


def require_phone(handler):
    def wrapper(update: Update, context: CallbackContext, dto: HasPhoneDto):
        try:
            validate_has_phone(dto)
        except HasNotPhoneError:
            return error(ERR_HAS_NO_PERMISSION)
        return handler(update, context, dto)

    return wrapper


def bot_endpoint(handler):
    def wrapper(update: Update, context: CallbackContext):
        result = handler(update, context)
        update.message.reply_text(result.text, **result.kwargs)

    return wrapper


def require_args(args_len, cmd_help=None):
    cmd_help = HELP if not cmd_help else cmd_help
    args_len = [args_len] if isinstance(args_len, int) else args_len
    if any(not isinstance(x, int) for x in args_len):
        raise TypeError

    def wrapper(handler):
        def wrpr(update: Update, context: CallbackContext):
            if len(context.args) not in args_len:
                return error(ERR_WRONG_COUNT_ARGUMENTS.format(cmd_help.hint))
            return handler(update, context)

        return wrpr

    return wrapper


class remove_message_after_send:
    def __init__(self, update: Update, context: CallbackContext):
        self.update = update
        self.context = context

    def __enter__(self):
        ...

    def __exit__(self, *args):
        try_delete_message(self.update, self.context)
