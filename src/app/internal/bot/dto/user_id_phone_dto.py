from app.internal.bot.dto.has_phone_dto import HasPhoneDto


class UserIdPhoneDto(HasPhoneDto):
    def __init__(self, user_id, phone_number):
        super().__init__(phone_number)
        self.id = user_id
