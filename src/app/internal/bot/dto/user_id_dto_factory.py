from app.internal.bot.dto.dto_factory import DtoFactory
from app.internal.bot.dto.user_id_dto import UserIdDto
from app.internal.users.db.models import AuthenticatedUser


class UserIdDtoFactory(DtoFactory):
    def get_fields(self):
        return DtoFactory.from_fields(AuthenticatedUser.id)

    def from_values(self, tup):
        return UserIdDto(*tup)
