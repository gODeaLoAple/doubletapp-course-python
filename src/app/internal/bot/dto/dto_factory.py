class DtoFactory:
    def get_fields(self):
        ...

    def from_values(self, tup):
        ...

    @staticmethod
    def from_fields(*fields):
        return [x.field.name for x in fields]
