from app.internal.bot.dto.dto_factory import DtoFactory
from app.internal.bot.dto.user_id_phone_dto import UserIdPhoneDto
from app.internal.users.db.models import AuthenticatedUser


class UserIdPhoneDtoFactory(DtoFactory):
    def get_fields(self):
        return DtoFactory.from_fields(AuthenticatedUser.id, AuthenticatedUser.phone)

    def from_values(self, tup):
        return UserIdPhoneDto(tup[0], tup[1])
