from app.internal.bot.dto.dto_factory import DtoFactory
from app.internal.bot.dto.user_full_dto import UserFullDto
from app.internal.users.db.models import AuthenticatedUser


class UserFullDtoFactory(DtoFactory):
    def get_fields(self):
        return DtoFactory.from_fields(
            AuthenticatedUser.id,
            AuthenticatedUser.telegram_id,
            AuthenticatedUser.first_name,
            AuthenticatedUser.last_name,
            AuthenticatedUser.username,
            AuthenticatedUser.phone,
        )

    def from_values(self, tup):
        return UserFullDto(*tup)
