from app.internal.bot.dto.dto_factory import DtoFactory
from app.internal.bot.dto.user_id_phone_username_dto import UserIdPhoneUsernameDto
from app.internal.users.db.models import AuthenticatedUser


class UserIdPhoneUsernameDtoFactory(DtoFactory):
    def get_fields(self):
        return DtoFactory.from_fields(AuthenticatedUser.id, AuthenticatedUser.phone, AuthenticatedUser.username)

    def from_values(self, tup):
        return UserIdPhoneUsernameDto(*tup)
