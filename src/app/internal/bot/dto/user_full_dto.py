from app.internal.bot.dto.has_phone_dto import HasPhoneDto


class UserFullDto(HasPhoneDto):
    def __init__(self, user_id, telegram_id, first_name, last_name, username, phone_number):
        super().__init__(phone_number)
        self.id = user_id
        self.telegram_id = telegram_id
        self.first_name = first_name
        self.last_name = last_name
        self.username = username
