from app.internal.bot.dto.user_id_phone_dto import UserIdPhoneDto


class UserIdPhoneUsernameDto(UserIdPhoneDto):
    def __init__(self, user_id, phone_number, username):
        super().__init__(user_id, phone_number)
        self.username = username
