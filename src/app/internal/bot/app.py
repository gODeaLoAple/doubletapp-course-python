import time

import telegram
from telegram.ext import CommandHandler

from app.internal.bot.commands import (
    ADD_FAVORITE,
    DELETE_FAVORITE,
    HELP,
    LOGIN,
    ME,
    MONEY,
    SET_PHONE,
    SHOW_FAVORITES,
    SHOW_TRANSACTIONS,
    START,
    STATEMENT,
    TRANSFER_MONEY,
)
from app.internal.bot.handlers.add_favorite import add_favorite
from app.internal.bot.handlers.delete_favorite import delete_favorite
from app.internal.bot.handlers.handle_error import handle_error
from app.internal.bot.handlers.help import handle_help
from app.internal.bot.handlers.login import login
from app.internal.bot.handlers.me import me
from app.internal.bot.handlers.money import money
from app.internal.bot.handlers.set_phone import set_phone
from app.internal.bot.handlers.show_favorites import show_favorites
from app.internal.bot.handlers.show_transactions import show_transactions
from app.internal.bot.handlers.start import start
from app.internal.bot.handlers.statement import statement
from app.internal.bot.handlers.transfer_money import transfer_money
from config.settings import (
    BOT_TOKEN,
    DEBUG,
    WEBHOOK_CERT_PATH,
    WEBHOOK_HOST,
    WEBHOOK_KEY_PATH,
    WEBHOOK_LISTEN,
    WEBHOOK_PORT,
)


def configure_dispatcher(dispatcher):
    handlers = [
        CommandHandler(HELP.name, handle_help),
        CommandHandler(START.name, start),
        CommandHandler(SET_PHONE.name, set_phone),
        CommandHandler(ME.name, me),
        CommandHandler(MONEY.name, money),
        CommandHandler(TRANSFER_MONEY.name, transfer_money),
        CommandHandler(ADD_FAVORITE.name, add_favorite),
        CommandHandler(DELETE_FAVORITE.name, delete_favorite),
        CommandHandler(SHOW_FAVORITES.name, show_favorites),
        CommandHandler(SHOW_TRANSACTIONS.name, show_transactions),
        CommandHandler(STATEMENT.name, statement),
        CommandHandler(LOGIN.name, login),
    ]
    dispatcher.add_error_handler(handle_error)
    for handler in handlers:
        dispatcher.add_handler(handler)


def run_bot_polling():
    updater = telegram.ext.Updater(token=BOT_TOKEN)
    dispatcher = updater.dispatcher
    configure_dispatcher(dispatcher)
    updater.start_polling()
    updater.idle()


def run_bot_webhooks():
    updater = telegram.ext.Updater(token=BOT_TOKEN, use_context=True)
    dispatcher = updater.dispatcher
    configure_dispatcher(dispatcher)
    webhook_url = f"https://{WEBHOOK_HOST}:{WEBHOOK_PORT}/{BOT_TOKEN}"
    updater.start_webhook(
        listen=WEBHOOK_LISTEN,
        port=WEBHOOK_PORT,
        url_path=BOT_TOKEN,
        key=WEBHOOK_KEY_PATH,
        cert=WEBHOOK_CERT_PATH,
        webhook_url=webhook_url,
    )
    time.sleep(1)
    updater.bot.set_webhook(webhook_url)
    updater.idle()


def run_bot():
    if DEBUG:
        run_bot_polling()
    else:
        run_bot_webhooks()
