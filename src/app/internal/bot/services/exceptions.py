class HasNotPhoneError(Exception):
    ...


class UserCardNotFoundError(Exception):
    ...


class NotEnoughMoneyError(Exception):
    ...


class SelfTransferError(Exception):
    ...


class FavoriteNotFoundError(Exception):
    ...


class SelfFavoriteError(Exception):
    ...


class FavoriteAlreadyAdded(Exception):
    ...


class FavoriteNotAddedError(Exception):
    ...


class IncorrectUserDataError(Exception):
    ...


class PhoneNumberAlreadyExistsError(Exception):
    ...


class IncorrectPhoneNumberError(Exception):
    ...


class IncorrectTransferMoneyError(Exception):
    ...


class TargetBankAccountNotFoundError(Exception):
    ...


class TargetBankAccountNotSpecifiedError(Exception):
    ...


class TargetNotFavoriteError(Exception):
    ...


class UserBankAccountNotFoundError(Exception):
    ...


class UserBankAccountNotSpecifiedError(Exception):
    ...


class UserHasNotBankAccountError(Exception):
    ...


class AuthenticatedUserNotFoundError(Exception):
    ...


class RefreshTokenNotRecognizedError(Exception):
    ...


class RefreshTokenHasBeenRevokedError(Exception):
    ...


class RefreshTokenExpiredError(Exception):
    ...


class IncorrectUserPasswordError(Exception):
    ...
