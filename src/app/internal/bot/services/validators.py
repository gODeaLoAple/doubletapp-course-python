﻿from app.internal.bot.dto.has_phone_dto import HasPhoneDto
from app.internal.bot.services.exceptions import HasNotPhoneError
from app.internal.users.db.models import AuthenticatedUser


def validate_has_phone(user: HasPhoneDto):
    if not user.phone_number:
        raise HasNotPhoneError


def validate_user_phone(user: AuthenticatedUser):
    if not user.phone:
        raise HasNotPhoneError
