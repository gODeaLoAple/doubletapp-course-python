from app.internal.bank.db.repositories import CardRepository
from app.internal.bank.domain.services import CardService

card_repo = CardRepository()
card_service = CardService(card_repo)


def get_card_balance(user_id, card_number):
    return card_service.get_card_balance(user_id, card_number).balance
