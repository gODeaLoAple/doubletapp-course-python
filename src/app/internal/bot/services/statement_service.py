from datetime import datetime

from app.internal.bank.db.repositories import BankAccountRepository, CardRepository, TransactionRepository
from app.internal.bank.domain.services import StatementService, TransactionService

bank_account_repo = BankAccountRepository()
card_repo = CardRepository()
transaction_repo = TransactionRepository()
transaction_service = TransactionService(transaction_repo)
statement_service = StatementService(bank_account_repo, card_repo, transaction_service)


def get_statement(user_id: int, card_or_account_id: str, start_date: datetime):
    return statement_service.get_statement(user_id, card_or_account_id, start_date)
