from datetime import datetime
from typing import List

from app.internal.bank.db.repositories import TransactionRepository
from app.internal.bank.domain.services import TransactionService

transaction_repo = TransactionRepository()
transaction_service = TransactionService(transaction_repo)


def get_transaction_history(user_id) -> List[str]:
    return transaction_service.get_transaction_history(user_id)


def get_last_transactions(bank_account_id: int, start_date: datetime):
    return transaction_service.get_last_transactions(bank_account_id, start_date)
