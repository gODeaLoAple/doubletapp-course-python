from app.internal.auth.db.repositories import TokenRepository
from app.internal.auth.domain.entities import JWTokensSchemaOut
from app.internal.auth.domain.services import AuthService
from app.internal.users.db.repositories import FavoriteRepository, UserRepository
from app.internal.users.domain.services import UserService

user_repo = UserRepository()
favorite_repo = FavoriteRepository()
token_repo = TokenRepository()
auth_service = AuthService(user_repo, token_repo)
user_service = UserService(user_repo)


def update_password(telegram_id, password, old_password):
    user_service.update_password(telegram_id, password, old_password)


def login(username: str, password: str) -> JWTokensSchemaOut:
    return auth_service.login(username, password)


def refresh(raw_refresh_token: str | None):
    return auth_service.refresh(raw_refresh_token)
