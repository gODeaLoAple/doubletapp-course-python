from app.internal.users.db.repositories import FavoriteRepository, UserRepository
from app.internal.users.domain.services import UserService

user_repo = UserRepository()
favorite_repo = FavoriteRepository()
user_service = UserService(user_repo)


def get_user_fields_by_telegram(telegram_id, fields):
    return user_service.get_user_fields_by_telegram(telegram_id, fields)


def save_user(telegram_id, first_name, last_name, username, password):
    return user_service.save_user(telegram_id, first_name, last_name, username, password)


def set_user_phone(user_id, phone_number: str):
    return user_service.set_user_phone(user_id, phone_number)


def get_user_info(user_id: int):
    return user_service.get_user_info(user_id)
