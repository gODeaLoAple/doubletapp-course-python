from app.internal.users.db.repositories import FavoriteRepository, UserRepository
from app.internal.users.domain.services import FavoriteService

user_repo = UserRepository()
favorite_repo = FavoriteRepository()
user_service = FavoriteService(user_repo, favorite_repo)


def add_favorite_by_username(user_id, favorite_username):
    return user_service.add_favorite_by_username(user_id, favorite_username)


def remove_favorite_by_username(user_id, favorite_username):
    return user_service.remove_favorite_by_username(user_id, favorite_username)


def get_favorites_of_user(user_id):
    return user_service.get_favorites_of_user(user_id)


def is_favorite_for(user_id, other_user_id):
    return user_service.is_favorite_for(user_id, other_user_id)
