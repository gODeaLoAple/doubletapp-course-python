from app.internal.bank.db.repositories import BankAccountRepository, TransferMoneyRepository
from app.internal.bank.domain.services import TransferService
from app.internal.users.db.repositories import FavoriteRepository, UserRepository
from app.internal.users.domain.services import FavoriteService

bank_account_repo = BankAccountRepository()
user_repo = UserRepository()
favorite_repo = FavoriteRepository()
favorite_service = FavoriteService(user_repo, favorite_repo)
transfer_money_repo = TransferMoneyRepository()
transfer_money_service = TransferService(user_repo, bank_account_repo, transfer_money_repo, favorite_service)


def transfer_money_to_bank_account(user_id, target_description: str, money, user_bank_or_card=None):
    return transfer_money_service.transfer_money_to_bank_account(user_id, target_description, money, user_bank_or_card)
