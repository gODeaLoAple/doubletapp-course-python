from app.internal.utils.messages import (
    CMD_ADD_FAVORITE,
    CMD_DELETE_FAVORITE,
    CMD_HELP,
    CMD_LOGIN,
    CMD_ME,
    CMD_MONEY,
    CMD_SET_PHONE,
    CMD_SHOW_FAVORITES,
    CMD_SHOW_TRANSACTIONS,
    CMD_START,
    CMD_STATEMENT,
    CMD_TRANSFER_MONEY,
)


class CommandInfo:
    def __init__(self, name, description, args=None):
        self.name = name
        self.description = description
        self.hint = f"/{name}" if args is None else f"/{name} {args}"

    def __str__(self):
        return self.name


HELP = CommandInfo("help", CMD_HELP)
START = CommandInfo("start", CMD_START, "[<password>]")
SET_PHONE = CommandInfo("set_phone", CMD_SET_PHONE, "<phone_number>")
ME = CommandInfo("me", CMD_ME)
MONEY = CommandInfo("money", CMD_MONEY, "<card_number>")
TRANSFER_MONEY = CommandInfo(
    "transfer_money",
    CMD_TRANSFER_MONEY,
    "<card_number|bank_account|telegram> <money> [<your_card_number|your_bank_account>]",
)
ADD_FAVORITE = CommandInfo("add_favorite", CMD_ADD_FAVORITE, "<username>")
DELETE_FAVORITE = CommandInfo("delete_favorite", CMD_DELETE_FAVORITE, "<username>")
SHOW_FAVORITES = CommandInfo("show_favorites", CMD_SHOW_FAVORITES)
SHOW_TRANSACTIONS = CommandInfo("show_transactions", CMD_SHOW_TRANSACTIONS)
STATEMENT = CommandInfo("statement", CMD_STATEMENT, "<bank_account|card_number>")
LOGIN = CommandInfo("login", CMD_LOGIN, "<new_password> [<old_password>]")
