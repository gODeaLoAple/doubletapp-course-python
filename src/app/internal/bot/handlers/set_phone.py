from telegram import Update
from telegram.ext import CallbackContext

from app.internal.bot.commands import SET_PHONE, START
from app.internal.bot.dto.user_id_dto import UserIdDto
from app.internal.bot.dto.user_id_dto_factory import UserIdDtoFactory
from app.internal.bot.services.exceptions import IncorrectPhoneNumberError, PhoneNumberAlreadyExistsError
from app.internal.bot.services.user_service import set_user_phone
from app.internal.bot.utils.result import error, ok
from app.internal.bot.utils.update_handler_decorators import bot_endpoint, require_args, require_dto
from app.internal.users.db.models import AuthenticatedUser
from app.internal.utils.messages import (
    ERR_NOT_CORRECT_PHONE_NUMBER,
    ERR_PHONE_ALREADY_EXISTS,
    ERR_USER_NOT_EXIST,
    INFO_PHONE_SET,
)


@bot_endpoint
@require_args(1, SET_PHONE)
@require_dto(UserIdDtoFactory())
def set_phone(update: Update, context: CallbackContext, dto: UserIdDto):
    phone_number = context.args[0]

    try:
        set_user_phone(dto.id, phone_number)
    except AuthenticatedUser.DoesNotExist:
        return error(ERR_USER_NOT_EXIST.format(START.hint))
    except IncorrectPhoneNumberError:
        return error(ERR_NOT_CORRECT_PHONE_NUMBER)
    except PhoneNumberAlreadyExistsError:
        return error(ERR_PHONE_ALREADY_EXISTS)

    return ok(INFO_PHONE_SET)
