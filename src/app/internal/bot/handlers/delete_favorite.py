from telegram import Update
from telegram.ext import CallbackContext

from app.internal.bot.commands import DELETE_FAVORITE
from app.internal.bot.dto.user_id_phone_dto import UserIdPhoneDto
from app.internal.bot.dto.user_id_phone_dto_factory import UserIdPhoneDtoFactory
from app.internal.bot.services.exceptions import FavoriteNotAddedError, FavoriteNotFoundError
from app.internal.bot.services.favorite_service import remove_favorite_by_username
from app.internal.bot.utils.result import error, ok
from app.internal.bot.utils.update_handler_decorators import bot_endpoint, require_args, require_dto, require_phone
from app.internal.utils.messages import ERR_FAVORITE_NOT_ADDED, ERR_FAVORITE_NOT_FOUND, INFO_DELETE_FAVORITE_SUCCESS
from app.internal.utils.parse import parse_telegram_username


@bot_endpoint
@require_args(1, DELETE_FAVORITE)
@require_dto(UserIdPhoneDtoFactory())
@require_phone
def delete_favorite(update: Update, context: CallbackContext, dto: UserIdPhoneDto):
    args = context.args

    favorite_username = parse_telegram_username(args[0])
    try:
        remove_favorite_by_username(dto.id, favorite_username)
    except FavoriteNotFoundError:
        return error(ERR_FAVORITE_NOT_FOUND)
    except FavoriteNotAddedError:
        return error(ERR_FAVORITE_NOT_ADDED)

    return ok(INFO_DELETE_FAVORITE_SUCCESS)
