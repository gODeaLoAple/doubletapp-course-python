from django.utils import timezone
from telegram import Update
from telegram.ext import CallbackContext

from app.internal.bank.db.models import BankAccount
from app.internal.bank.domain.entities import TransactionSchema
from app.internal.bot.commands import STATEMENT
from app.internal.bot.dto.user_id_phone_username_dto import UserIdPhoneUsernameDto
from app.internal.bot.dto.user_id_phone_username_dto_factory import UserIdPhoneUsernameDtoFactory
from app.internal.bot.services.statement_service import get_statement
from app.internal.bot.utils.result import error, ok
from app.internal.bot.utils.update_handler_decorators import bot_endpoint, require_args, require_dto, require_phone
from app.internal.utils.format import MarkdownV2, format_card_name, format_date, format_money_row
from app.internal.utils.messages import (
    ERR_BANK_ACCOUNT_NOT_FOUND,
    INFO_STATEMENT_ACCOUNT_ID,
    INFO_STATEMENT_CARDS,
    INFO_STATEMENT_OPERATIONS_HISTORY,
    INFO_STATEMENT_OPERATIONS_HISTORY_EMPTY,
    INFO_STATEMENT_RECEIVED,
    INFO_STATEMENT_SENT,
)


@bot_endpoint
@require_args(1, STATEMENT)
@require_dto(UserIdPhoneUsernameDtoFactory())
@require_phone
def statement(update: Update, context: CallbackContext, dto: UserIdPhoneUsernameDto):
    card_or_id = context.args[0]
    before_30_days = timezone.now() - timezone.timedelta(30)

    try:
        bank_account_id, cards_names, transactions = get_statement(dto.id, card_or_id, before_30_days)
    except BankAccount.DoesNotExist:
        return error(ERR_BANK_ACCOUNT_NOT_FOUND)

    text = create_statement_text(dto.username, bank_account_id, cards_names, transactions)
    return ok(text, parse_mode=MarkdownV2.MODE)


def create_statement_text(username, bank_account_id, cards_names, transactions):
    cards_text = "\n".join(f"*{format_card_name(name)}*" for name in cards_names)
    transaction_formatter = create_transaction_formatter(username)
    transactions_text = "\n".join(map(transaction_formatter, transactions))
    header = f"""{escape(INFO_STATEMENT_ACCOUNT_ID)}{bank_account_id}
{escape(INFO_STATEMENT_CARDS)}
{cards_text}"""
    footer = (
        f"""{escape(INFO_STATEMENT_OPERATIONS_HISTORY)}
{transactions_text}"""
        if transactions_text
        else escape(INFO_STATEMENT_OPERATIONS_HISTORY_EMPTY)
    )
    return f"{header}\n{footer}"


def create_transaction_formatter(username: str):
    def wrapper(transaction: TransactionSchema):
        return format_transaction(transaction, username)

    return wrapper


def format_transaction(transaction: TransactionSchema, username: str):
    money_text = format_money_row(transaction.money)
    date_text = format_date(transaction.date)
    if transaction.recipient_username == username:
        return escape(INFO_STATEMENT_RECEIVED.format(transaction.sender_username, money_text, date_text))
    return escape(INFO_STATEMENT_SENT.format(transaction.recipient_username, money_text, date_text))


def escape(text):
    return MarkdownV2.escape(text)
