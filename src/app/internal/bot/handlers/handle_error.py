from telegram import Update
from telegram.ext import CallbackContext

from app.internal.utils.format import MarkdownV2
from app.internal.utils.messages import ERR_MESSAGE


def handle_error(update: Update, context: CallbackContext):
    try:
        update.message.reply_text(MarkdownV2.escape(ERR_MESSAGE), parse_mode=MarkdownV2.MODE)
    except Exception as e:
        print(e)
