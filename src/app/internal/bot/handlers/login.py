from telegram import Update
from telegram.ext import CallbackContext

from app.internal.bot.commands import LOGIN
from app.internal.bot.dto import UserIdPhoneUsernameDtoFactory
from app.internal.bot.dto.user_id_phone_username_dto import UserIdPhoneUsernameDto
from app.internal.bot.services.auth_service import update_password
from app.internal.bot.services.exceptions import IncorrectUserPasswordError
from app.internal.bot.utils.result import error, ok
from app.internal.bot.utils.update_handler_decorators import (
    bot_endpoint,
    remove_message_after_send,
    require_args,
    require_dto,
    require_phone,
)


@bot_endpoint
@require_args(2, LOGIN)
@require_dto(UserIdPhoneUsernameDtoFactory())
@require_phone
def login(update: Update, context: CallbackContext, dto: UserIdPhoneUsernameDto):
    telegram_id = update.effective_user.id
    args = context.args
    password = args[0]

    old_password = args[1] if len(args) == 2 else None
    try:
        with remove_message_after_send(update, context):
            update_password(telegram_id, password, old_password)
    except IncorrectUserPasswordError:
        return error("Incorrect password.")
    return ok("Password successfully updated.")
