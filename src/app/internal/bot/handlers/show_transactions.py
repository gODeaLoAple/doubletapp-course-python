from telegram import Update
from telegram.ext import CallbackContext

from app.internal.bot.commands import SHOW_TRANSACTIONS
from app.internal.bot.dto.user_id_phone_dto import UserIdPhoneDto
from app.internal.bot.dto.user_id_phone_dto_factory import UserIdPhoneDtoFactory
from app.internal.bot.services.transactions_service import get_transaction_history
from app.internal.bot.utils.result import ok
from app.internal.bot.utils.update_handler_decorators import bot_endpoint, require_args, require_dto, require_phone
from app.internal.utils.messages import (
    INFO_TRANSACTION_HISTORY_EMPTY,
    INFO_TRANSACTION_HISTORY_ITEM,
    INFO_TRANSACTION_HISTORY_LIST,
)


@bot_endpoint
@require_args(0, SHOW_TRANSACTIONS)
@require_dto(UserIdPhoneDtoFactory())
@require_phone
def show_transactions(update: Update, context: CallbackContext, dto: UserIdPhoneDto):
    usernames = list(get_transaction_history(dto.id))
    if usernames:
        return ok(create_transactions_text(usernames))
    return ok(INFO_TRANSACTION_HISTORY_EMPTY)


def create_transactions_text(usernames):
    header = INFO_TRANSACTION_HISTORY_LIST
    list_text = "\n".join(INFO_TRANSACTION_HISTORY_ITEM.format(name) for name in usernames)
    return f"{header}\n{list_text}"
