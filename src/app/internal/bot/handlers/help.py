from telegram import Update
from telegram.ext import CallbackContext

from app.internal.bot.commands import (
    ADD_FAVORITE,
    DELETE_FAVORITE,
    HELP,
    LOGIN,
    ME,
    MONEY,
    SET_PHONE,
    SHOW_FAVORITES,
    SHOW_TRANSACTIONS,
    START,
    STATEMENT,
    TRANSFER_MONEY,
)
from app.internal.bot.utils.result import ok
from app.internal.bot.utils.update_handler_decorators import bot_endpoint, require_args
from app.internal.utils.format import MarkdownV2


@bot_endpoint
@require_args(0, HELP)
def handle_help(update: Update, context: CallbackContext):
    cmd = [
        HELP,
        ME,
        MONEY,
        SET_PHONE,
        START,
        TRANSFER_MONEY,
        ADD_FAVORITE,
        DELETE_FAVORITE,
        SHOW_FAVORITES,
        SHOW_TRANSACTIONS,
        STATEMENT,
        LOGIN,
    ]

    text = "\n".join(f"*{MarkdownV2.escape(c.hint)}* \\- {MarkdownV2.escape(c.description)}" for c in cmd)

    return ok(text, parse_mode=MarkdownV2.MODE)
