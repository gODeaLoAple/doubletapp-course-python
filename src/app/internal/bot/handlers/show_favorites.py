from telegram import Update
from telegram.ext import CallbackContext

from app.internal.bot.commands import SHOW_FAVORITES
from app.internal.bot.dto.user_id_phone_dto import UserIdPhoneDto
from app.internal.bot.dto.user_id_phone_dto_factory import UserIdPhoneDtoFactory
from app.internal.bot.services.favorite_service import get_favorites_of_user
from app.internal.bot.utils.result import ok
from app.internal.bot.utils.update_handler_decorators import bot_endpoint, require_args, require_dto, require_phone
from app.internal.utils.messages import INFO_FAVORITES_LIST, INFO_FAVORITES_LIST_EMPTY


@bot_endpoint
@require_args(0, SHOW_FAVORITES)
@require_dto(UserIdPhoneDtoFactory())
@require_phone
def show_favorites(update: Update, context: CallbackContext, dto: UserIdPhoneDto):
    username = get_favorites_of_user(dto.id).favorites
    if len(username) > 0:
        return ok(f"{INFO_FAVORITES_LIST}\n" + "\n".join(f"@{f}" for f in username))

    return ok(INFO_FAVORITES_LIST_EMPTY)
