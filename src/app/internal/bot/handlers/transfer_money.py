from telegram import Update
from telegram.ext import CallbackContext

from app.internal.bot.commands import TRANSFER_MONEY
from app.internal.bot.dto.user_id_phone_dto import UserIdPhoneDto
from app.internal.bot.dto.user_id_phone_dto_factory import UserIdPhoneDtoFactory
from app.internal.bot.services.exceptions import (
    IncorrectTransferMoneyError,
    NotEnoughMoneyError,
    TargetBankAccountNotFoundError,
    TargetBankAccountNotSpecifiedError,
    TargetNotFavoriteError,
    UserBankAccountNotFoundError,
    UserBankAccountNotSpecifiedError,
    UserHasNotBankAccountError,
)
from app.internal.bot.services.transfer_money_service import transfer_money_to_bank_account
from app.internal.bot.utils.result import error, ok
from app.internal.bot.utils.update_handler_decorators import bot_endpoint, require_args, require_dto, require_phone
from app.internal.utils.messages import (
    ERR_BANK_ACCOUNT_NOT_FOUND,
    ERR_NEGATIVE_MONEY,
    ERR_NOT_ENOUGH_MONEY,
    ERR_NOT_FAVORITE,
    ERR_NOT_MONEY_WHEN_TRANSFER_MONEY,
    ERR_TARGET_HAS_MANY_BANK_ACCOUNTS_WITH_HINT,
    ERR_USER_BANK_ACCOUNT_NOT_FOUND,
    ERR_USER_HAS_MANY_BANK_ACCOUNTS_WITH_HINT,
    ERR_USER_HAS_NOT_BANK_ACCOUNT,
    INFO_TRANSFER_MONEY_SUCCESS,
)
from app.internal.utils.parse import try_parse_int


@bot_endpoint
@require_args([2, 3], TRANSFER_MONEY)
@require_dto(UserIdPhoneDtoFactory())
@require_phone
def transfer_money(update: Update, context: CallbackContext, dto: UserIdPhoneDto):
    args = context.args

    target_username_or_bank_or_card = args[0]
    money_input = args[1]
    user_bank_or_card = args[2] if len(args) == 3 else None

    money_sum, parsed = try_parse_int(money_input)
    if not parsed:
        return error(ERR_NOT_MONEY_WHEN_TRANSFER_MONEY)

    try:
        transfer_money_to_bank_account(dto.id, target_username_or_bank_or_card, money_sum, user_bank_or_card)
    except UserHasNotBankAccountError:
        return error(ERR_USER_HAS_NOT_BANK_ACCOUNT)
    except UserBankAccountNotSpecifiedError:
        return error(ERR_USER_HAS_MANY_BANK_ACCOUNTS_WITH_HINT)
    except UserBankAccountNotFoundError:
        return error(ERR_USER_BANK_ACCOUNT_NOT_FOUND)
    except TargetNotFavoriteError:
        return error(ERR_NOT_FAVORITE)
    except TargetBankAccountNotSpecifiedError:
        return error(ERR_TARGET_HAS_MANY_BANK_ACCOUNTS_WITH_HINT)
    except TargetBankAccountNotFoundError:
        return error(ERR_BANK_ACCOUNT_NOT_FOUND)
    except IncorrectTransferMoneyError:
        return error(ERR_NEGATIVE_MONEY)
    except NotEnoughMoneyError:
        return error(ERR_NOT_ENOUGH_MONEY)
    return ok(INFO_TRANSFER_MONEY_SUCCESS)
