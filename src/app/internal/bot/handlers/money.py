from telegram import Update
from telegram.ext import CallbackContext

from app.internal.bank.db.models import Card
from app.internal.bot.commands import MONEY
from app.internal.bot.dto.user_id_phone_dto import UserIdPhoneDto
from app.internal.bot.dto.user_id_phone_dto_factory import UserIdPhoneDtoFactory
from app.internal.bot.services.card_service import get_card_balance
from app.internal.bot.services.exceptions import UserCardNotFoundError
from app.internal.bot.utils.result import error, ok
from app.internal.bot.utils.update_handler_decorators import bot_endpoint, require_args, require_dto, require_phone
from app.internal.utils.format import MarkdownV2, format_money
from app.internal.utils.messages import ERR_CARD_NOT_FOUND, INFO_PERSON_CARD_BALANCE


@bot_endpoint
@require_args(1, MONEY)
@require_dto(UserIdPhoneDtoFactory())
@require_phone
def money(update: Update, context: CallbackContext, dto: UserIdPhoneDto):
    args = context.args

    try:
        balance = get_card_balance(dto.id, args[0])
    except UserCardNotFoundError:
        return error(ERR_CARD_NOT_FOUND)

    text = MarkdownV2.escape(INFO_PERSON_CARD_BALANCE)
    return ok(f"{text}: *{format_money(balance)}*", parse_mode=MarkdownV2.MODE)
