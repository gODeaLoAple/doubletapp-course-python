from telegram import Update
from telegram.ext import CallbackContext

from app.internal.bot.commands import ADD_FAVORITE
from app.internal.bot.dto.user_id_phone_dto import UserIdPhoneDto
from app.internal.bot.dto.user_id_phone_dto_factory import UserIdPhoneDtoFactory
from app.internal.bot.services.exceptions import FavoriteAlreadyAdded, FavoriteNotFoundError, SelfFavoriteError
from app.internal.bot.services.favorite_service import add_favorite_by_username
from app.internal.bot.utils.result import error, ok
from app.internal.bot.utils.update_handler_decorators import bot_endpoint, require_args, require_dto, require_phone
from app.internal.utils.messages import (
    ERR_FAVORITE_ALREADY_ADDED,
    ERR_FAVORITE_NOT_FOUND,
    ERR_SELF_FAVORITE,
    INFO_ADD_FAVORITE_SUCCESS,
)
from app.internal.utils.parse import parse_telegram_username


@bot_endpoint
@require_args(1, ADD_FAVORITE)
@require_dto(UserIdPhoneDtoFactory())
@require_phone
def add_favorite(update: Update, context: CallbackContext, dto: UserIdPhoneDto):
    args = context.args
    favorite_username = parse_telegram_username(args[0])

    try:
        add_favorite_by_username(dto.id, favorite_username)
    except FavoriteAlreadyAdded:
        return error(ERR_FAVORITE_ALREADY_ADDED)
    except SelfFavoriteError:
        return error(ERR_SELF_FAVORITE)
    except FavoriteNotFoundError:
        return error(ERR_FAVORITE_NOT_FOUND)

    return ok(INFO_ADD_FAVORITE_SUCCESS)
