from telegram import Update
from telegram.ext import CallbackContext

from app.internal.bot.commands import ME
from app.internal.bot.dto.user_full_dto import UserFullDto
from app.internal.bot.dto.user_full_dto_factory import UserFullDtoFactory
from app.internal.bot.utils.result import ok
from app.internal.bot.utils.update_handler_decorators import bot_endpoint, require_args, require_dto, require_phone
from app.internal.utils.format import MarkdownV2
from app.internal.utils.messages import (
    INFO_PERSONAL_FIRST_NAME,
    INFO_PERSONAL_ID,
    INFO_PERSONAL_LAST_NAME,
    INFO_PERSONAL_PHONE,
    INFO_PERSONAL_TELEGRAM_ID,
    INFO_PERSONAL_USERNAME,
)


@bot_endpoint
@require_args(0, ME)
@require_dto(UserFullDtoFactory())
@require_phone
def me(update: Update, context: CallbackContext, dto: UserFullDto):
    text = format_me_text(dto.id, dto.telegram_id, dto.first_name, dto.last_name or "", dto.username, dto.phone_number)

    return ok(text, parse_mode=MarkdownV2.MODE)


def format_me_text(user_id, telegram_id, first_name, last_name, username, phone_number):
    return f"""*{INFO_PERSONAL_ID}*: {user_id}
*{INFO_PERSONAL_TELEGRAM_ID}*: {telegram_id}
*{INFO_PERSONAL_FIRST_NAME}*: {first_name}
*{INFO_PERSONAL_LAST_NAME}*: {last_name}
*{INFO_PERSONAL_USERNAME}*: @{username}
*{INFO_PERSONAL_PHONE}*: {phone_number}
"""
