from telegram import Update
from telegram.ext import CallbackContext

from app.internal.bot.commands import START
from app.internal.bot.services.exceptions import IncorrectUserDataError
from app.internal.bot.services.user_service import save_user
from app.internal.bot.utils.result import error, ok
from app.internal.bot.utils.update_handler_decorators import bot_endpoint, remove_message_after_send, require_args
from app.internal.users.db.models import AuthenticatedUser
from app.internal.utils.messages import ERR_PASSWORD_REQUIRED, ERR_WORD, INFO_USER_SAVED, INFO_USER_UPDATED


@bot_endpoint
@require_args([0, 1], START)
def start(update: Update, context: CallbackContext):
    user = update.effective_user
    args = context.args
    password = args[0] if len(args) else None
    kwargs = dict(
        telegram_id=user.id,
        first_name=user.first_name,
        last_name=user.last_name,
        username=user.username,
        password=password,
    )
    try:
        if password is not None:
            with remove_message_after_send(update, context):
                saved = save_user(**kwargs)
        else:
            saved = save_user(**kwargs)
    except AuthenticatedUser.DoesNotExist:
        return error(ERR_PASSWORD_REQUIRED.format(START.hint))
    except IncorrectUserDataError:
        return error(ERR_WORD)
    return ok(INFO_USER_SAVED if saved else INFO_USER_UPDATED)
