def get_value_or_default(dct, key, default_value=None):
    return dct[key] if key in dct else default_value
