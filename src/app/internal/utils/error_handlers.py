from typing import Type

from django.http import HttpRequest
from ninja import NinjaAPI

from app.internal.bot.services.exceptions import (
    FavoriteAlreadyAdded,
    FavoriteNotAddedError,
    FavoriteNotFoundError,
    HasNotPhoneError,
    IncorrectTransferMoneyError,
    NotEnoughMoneyError,
    SelfFavoriteError,
    TargetBankAccountNotFoundError,
    TargetBankAccountNotSpecifiedError,
    TargetNotFavoriteError,
    UserBankAccountNotFoundError,
    UserBankAccountNotSpecifiedError,
    UserCardNotFoundError,
    UserHasNotBankAccountError,
)
from app.internal.utils.messages import (
    ERR_BANK_ACCOUNT_NOT_FOUND,
    ERR_CARD_NOT_FOUND,
    ERR_FAVORITE_ALREADY_ADDED,
    ERR_FAVORITE_NOT_ADDED,
    ERR_FAVORITE_NOT_FOUND,
    ERR_HAS_NO_PERMISSION,
    ERR_NEGATIVE_MONEY,
    ERR_NOT_ENOUGH_MONEY,
    ERR_NOT_FAVORITE,
    ERR_SELF_FAVORITE,
    ERR_TARGET_HAS_MANY_BANK_ACCOUNTS_WITH_HINT,
    ERR_USER_BANK_ACCOUNT_NOT_FOUND,
    ERR_USER_HAS_MANY_BANK_ACCOUNTS_WITH_HINT,
    ERR_USER_HAS_NOT_BANK_ACCOUNT,
)
from app.internal.utils.rest_exceptions import AccessDeniedError, UnauthorizedError


def configure_error_handlers(api: NinjaAPI):
    add_handler(api, UnauthorizedError, unauthorized_exception_handler)
    add_handler(api, AccessDeniedError, access_denied_exception_handler)
    add_domain_exceptions(api)


def add_handler(api, exc, handler):
    def wrapper(request, e):
        handler(request, e, api)

    api.add_exception_handler(exc, wrapper)


def unauthorized_exception_handler(request: HttpRequest, e: UnauthorizedError, api: NinjaAPI):
    return api.create_response(request, {"message": f"UnauthorizedException. {e.detail or ''}"}, status=401)


def access_denied_exception_handler(request: HttpRequest, e: AccessDeniedError, api: NinjaAPI):
    return api.create_response(request, {"message": f"AccessDeniedError. {e.detail or ''}"}, status=403)


def create_message_handler(message, api):
    def wrapped(request, e):
        return api.create_response(request, {"detail": message}, status=400)

    return wrapped


def add_domain_exceptions(api: NinjaAPI):
    mp = map_exception_to_message()
    for exception_type, message in mp.items():
        api.add_exception_handler(exception_type, create_message_handler(message, api))


def map_exception_to_message():
    return {
        FavoriteAlreadyAdded: ERR_FAVORITE_ALREADY_ADDED,
        SelfFavoriteError: ERR_SELF_FAVORITE,
        FavoriteNotFoundError: ERR_FAVORITE_NOT_FOUND,
        FavoriteNotAddedError: ERR_FAVORITE_NOT_ADDED,
        HasNotPhoneError: ERR_HAS_NO_PERMISSION,
        UserCardNotFoundError: ERR_CARD_NOT_FOUND,
        UserHasNotBankAccountError: ERR_USER_HAS_NOT_BANK_ACCOUNT,
        UserBankAccountNotSpecifiedError: ERR_USER_HAS_MANY_BANK_ACCOUNTS_WITH_HINT,
        UserBankAccountNotFoundError: ERR_USER_BANK_ACCOUNT_NOT_FOUND,
        TargetNotFavoriteError: ERR_NOT_FAVORITE,
        TargetBankAccountNotSpecifiedError: ERR_TARGET_HAS_MANY_BANK_ACCOUNTS_WITH_HINT,
        TargetBankAccountNotFoundError: ERR_BANK_ACCOUNT_NOT_FOUND,
        IncorrectTransferMoneyError: ERR_NEGATIVE_MONEY,
        NotEnoughMoneyError: ERR_NOT_ENOUGH_MONEY,
    }
