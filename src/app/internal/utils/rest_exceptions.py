from typing import Optional


class UnauthorizedError(Exception):
    def __init__(self, detail: Optional[str] = None):
        self.detail = detail


class AccessDeniedError(Exception):
    def __init__(self, detail: Optional[str]):
        self.detail = detail
