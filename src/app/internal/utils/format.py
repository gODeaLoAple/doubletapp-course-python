from telegram import ParseMode
from telegram.utils.helpers import escape_markdown


class MarkdownV2:
    MODE = ParseMode.MARKDOWN_V2

    @staticmethod
    def escape(text: str):
        return escape_markdown(text, version=2)


def format_card_name(card_name):
    return "{}{}{}{} {}{}{}{} {}{}{}{} {}{}{}{}".format(*card_name)


def format_money(money):
    return MarkdownV2.escape(format_money_row(money))


def format_money_row(money):
    return "${:,.2f}".format(money)


def format_date(date):
    return date.strftime("%H:%M:%S, %d.%m.%Y")
