from ninja import Schema


class Ok(Schema):
    ...


class ErrorResponse(Schema):
    message: str


class NotFound(ErrorResponse):
    ...


class Unauthorized(ErrorResponse):
    ...


class Forbidden(ErrorResponse):
    ...


class BadRequest(ErrorResponse):
    ...
