def try_parse_int(number: str):
    try:
        return int(number), True
    except ValueError:
        return None, False


def parse_telegram_username(username: str) -> str:
    return username[1:] if username.startswith("@") else username
