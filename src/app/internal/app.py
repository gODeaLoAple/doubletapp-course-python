from ninja import NinjaAPI

from app.internal.auth.app import configure_auth_api
from app.internal.bank.app import configure_bank_api
from app.internal.users.app import configure_users_api
from app.internal.utils.error_handlers import configure_error_handlers


def get_api():
    api = NinjaAPI(
        title="DT.EDU.BACKEND",
        version="1.0.0",
        auth=None,
    )
    configure_users_api(api)
    configure_auth_api(api)
    configure_bank_api(api)
    configure_error_handlers(api)
    return api
