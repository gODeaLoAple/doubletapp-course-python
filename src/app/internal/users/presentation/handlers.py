from django.http import HttpRequest
from ninja import Body

from app.internal.users.domain.entities import (
    FavoritesUsernamesSchemaOut,
    FavoriteUsernameSchemaIn,
    PhoneSetSchemaIn,
    UserSchemaOut,
)
from app.internal.users.domain.services import FavoriteService, UserService
from app.internal.utils.http_responses import Ok
from app.internal.utils.rest_exceptions import AccessDeniedError, UnauthorizedError


class UserHandlers:
    def __init__(self, user_service: UserService):
        self.user_service = user_service

    def me(self, request: HttpRequest) -> UserSchemaOut:
        user = request.user
        if user is None:
            raise UnauthorizedError()
        return self.user_service.get_user_info(user.id)

    def set_phone(self, request: HttpRequest, data: PhoneSetSchemaIn = Body(...)) -> Ok:
        user = request.user
        if user is None:
            raise UnauthorizedError()
        self.user_service.set_user_phone(user.id, data.phone)
        return Ok()


class FavoriteHandlers:
    def __init__(self, favorite_service: FavoriteService):
        self.favorite_service = favorite_service

    def put(self, request: HttpRequest, data: FavoriteUsernameSchemaIn = Body(...)) -> Ok:
        user = request.user
        if user is None:
            raise UnauthorizedError()
        self.favorite_service.add_favorite_by_username(user.id, data.username)
        return Ok()

    def delete(self, request: HttpRequest, data: FavoriteUsernameSchemaIn = Body(...)) -> Ok:
        user = request.user
        if user is None:
            raise UnauthorizedError()
        self.favorite_service.remove_favorite_by_username(user.id, data.username)
        return Ok()

    def get(self, request: HttpRequest) -> FavoritesUsernamesSchemaOut:
        user = request.user
        if user is None:
            raise UnauthorizedError()
        return self.favorite_service.get_favorites_of_user(user.id)
