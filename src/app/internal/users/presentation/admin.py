from django.contrib import admin
from django.urls import reverse
from django.utils.html import format_html

from app.internal.users.db.models import AuthenticatedUser, Favorite


@admin.register(AuthenticatedUser)
class AuthenticatedUserAdmin(admin.ModelAdmin):
    list_display = ("id", "telegram_id", "first_name", "last_name", "username", "phone", "password", "is_staff")


@admin.register(Favorite)
class FavoriteAdmin(admin.ModelAdmin):
    list_display = ("id", "view_user", "view_favorite_user")
    list_filter = ("user", "favorite_user")

    def view_user(self, obj):
        url = reverse("admin:app_user_change", args=[obj.user.id])
        return format_html('<a href="{}"> {} ({})</a>', url, obj.user.username, obj.user.id)

    view_user.short_description = "User"

    def view_favorite_user(self, obj):
        url = reverse("admin:app_user_change", args=[obj.favorite_user.id])
        return format_html('<a href="{}"> {} ({})</a>', url, obj.favorite_user.username, obj.favorite_user.id)

    view_favorite_user.short_description = "Favorite user"
