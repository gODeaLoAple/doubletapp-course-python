from ninja import NinjaAPI, Router

from app.internal.auth.presentation.middleware.http_jwt_auth import HTTPJWTAuth
from app.internal.users.domain.entities import FavoritesUsernamesSchemaOut, UserSchemaOut
from app.internal.users.presentation.handlers import FavoriteHandlers, UserHandlers
from app.internal.utils.http_responses import Forbidden, Ok, Unauthorized


def get_users_router(users_handlers: UserHandlers, favorite_handlers: FavoriteHandlers):
    router = Router(tags=["users"])

    router.add_api_operation(
        "me/",
        ["GET"],
        users_handlers.me,
        response={200: UserSchemaOut, 401: Unauthorized, 403: Forbidden},
        auth=[HTTPJWTAuth()],
    )

    router.add_api_operation(
        "phone/",
        ["POST"],
        users_handlers.set_phone,
        response={200: Ok, 401: Unauthorized, 403: Forbidden},
        auth=[HTTPJWTAuth()],
    )

    router.add_api_operation(
        "favorites/",
        ["PUT"],
        favorite_handlers.put,
        response={200: Ok, 401: Unauthorized, 403: Forbidden},
        auth=[HTTPJWTAuth()],
    )

    router.add_api_operation(
        "favorites/",
        ["DELETE"],
        favorite_handlers.delete,
        response={200: FavoritesUsernamesSchemaOut, 401: Unauthorized, 403: Forbidden},
        auth=[HTTPJWTAuth()],
    )

    router.add_api_operation(
        "favorites/",
        ["GET"],
        favorite_handlers.get,
        response={200: FavoritesUsernamesSchemaOut, 401: Unauthorized, 403: Forbidden},
        auth=[HTTPJWTAuth()],
    )

    return router


def add_users_router(api: NinjaAPI, users_handlers: UserHandlers, favorite_handlers: FavoriteHandlers):
    users_router = get_users_router(users_handlers, favorite_handlers)
    api.add_router("users", users_router)
