from ninja import NinjaAPI

from app.internal.auth.presentation.middleware.http_jwt_auth import HTTPJWTAuth
from app.internal.users.db.repositories import FavoriteRepository, UserRepository
from app.internal.users.domain.services import FavoriteService, UserService
from app.internal.users.presentation.handlers import FavoriteHandlers, UserHandlers
from app.internal.users.presentation.routers import add_users_router


def get_api():
    api = NinjaAPI(
        title="DT.EDU.BACKEND",
        version="1.0.0",
        auth=[HTTPJWTAuth()],
    )
    configure_users_api(api)
    return api


def configure_users_api(api: NinjaAPI):
    user_repo = UserRepository()
    user_service = UserService(user_repo)
    user_handlers = UserHandlers(user_service)
    favorites_repo = FavoriteRepository()

    favorites_service = FavoriteService(user_repo, favorites_repo)
    favorites_handlers = FavoriteHandlers(favorites_service)
    add_users_router(api, user_handlers, favorites_handlers)
