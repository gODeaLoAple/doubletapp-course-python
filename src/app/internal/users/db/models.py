from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import PermissionsMixin
from django.core.validators import RegexValidator
from django.db import models

_phone_regex = RegexValidator(regex=r"^\+?1?\d{8,15}$")


class AuthenticatedUserManager(BaseUserManager):
    def create_user(self, username, telegram_id, password, **extra_fields):
        if not username:
            raise ValueError("The username must be set")
        user = self.model(username=username, telegram_id=telegram_id, **extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, username, telegram_id, password, **extra_fields):
        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_superuser", True)
        extra_fields.setdefault("is_active", True)
        if extra_fields.get("is_staff") is not True:
            raise ValueError("Superuser must have is_staff=True.")
        if extra_fields.get("is_superuser") is not True:
            raise ValueError("Superuser must have is_staff=True.")
        return self.create_user(username, telegram_id, password, **extra_fields)


class AuthenticatedUser(AbstractBaseUser, PermissionsMixin):
    id = models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name="id")
    telegram_id = models.BigIntegerField(unique=True, default=0, verbose_name="telegram id")
    username = models.CharField(unique=True, max_length=32, verbose_name="username")
    first_name = models.CharField(max_length=64, verbose_name="first name")
    last_name = models.CharField(blank=True, max_length=64, verbose_name="last name")
    phone = models.CharField(
        max_length=16,
        unique=True,
        null=True,
        default=None,
        blank=True,
        validators=[_phone_regex],
        verbose_name="phone number",
    )
    is_active = models.BooleanField(default=True)
    is_superuser = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=True)
    date_joined = models.DateTimeField(auto_now=True)

    USERNAME_FIELD = "username"
    REQUIRED_FIELDS = ["telegram_id"]
    objects = AuthenticatedUserManager()

    def __str__(self):
        return f"{self.username} ({self.id})"

    class Meta:
        verbose_name = "User"
        verbose_name_plural = "Users"


class Favorite(models.Model):
    id = models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name="id")
    user = models.ForeignKey(AuthenticatedUser, on_delete=models.CASCADE, related_name="user", verbose_name="user")
    favorite_user = models.ForeignKey(
        AuthenticatedUser, on_delete=models.CASCADE, related_name="favorite_user", verbose_name="favorite_user"
    )

    class Meta:
        verbose_name = "Favorite"
        verbose_name_plural = "Favorites"
        constraints = [models.UniqueConstraint(fields=["user", "favorite_user"], name="IX_USER_FAVORITE_USER")]
