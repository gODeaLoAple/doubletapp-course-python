from typing import List, Tuple

from django.core.exceptions import ValidationError
from django.db import DataError, IntegrityError

from app.internal.bot.services.exceptions import (
    IncorrectPhoneNumberError,
    IncorrectUserDataError,
    PhoneNumberAlreadyExistsError,
)
from app.internal.users.db.models import AuthenticatedUser, Favorite
from app.internal.users.domain.services import IFavoriteRepository, IUserRepository


class UserRepository(IUserRepository):
    def get_user_by_telegram(self, telegram_id, fields: List[str]):
        return AuthenticatedUser.objects.filter(telegram_id=telegram_id).values_list(*fields).get()

    def save_user(
        self, telegram_id: int, first_name: str, last_name: str, username: str, password: str | None
    ) -> Tuple[bool, AuthenticatedUser]:
        defaults = dict(telegram_id=telegram_id, first_name=first_name, last_name=last_name, username=username)

        if password is None:
            if not AuthenticatedUser.objects.filter(telegram_id=telegram_id).exists():
                raise AuthenticatedUser.DoesNotExist
        try:
            user, created = AuthenticatedUser.objects.update_or_create(telegram_id=telegram_id, defaults=defaults)
            if password is not None:
                user.set_password(password)
            user.save()
        except IntegrityError:
            raise IncorrectUserDataError
        except DataError:
            raise IncorrectUserDataError
        except AuthenticatedUser.DoesNotExist:
            raise IncorrectUserDataError

        return user, created

    def set_phone(self, user_id: int, phone_number: str):
        user = AuthenticatedUser.objects.filter(id=user_id).get()
        user.phone = phone_number
        try:
            user.full_clean()
        except ValidationError as e:
            _reraise_set_phone_error(e)
        user.save()

    def get_user_by_id(self, user_id: int) -> AuthenticatedUser:
        return AuthenticatedUser.objects.filter(id=user_id).get()

    def find_user_by_username(self, username: str) -> AuthenticatedUser:
        return AuthenticatedUser.objects.filter(username=username).first()

    def get_user_by_telegram_id(self, telegram_id: str) -> AuthenticatedUser:
        return AuthenticatedUser.objects.filter(telegram_id=telegram_id).get()

    def get_user_id_by_username(self, username: str) -> int:
        return AuthenticatedUser.objects.filter(username=username).values_list("id", flat=True).get()

    def has_phone(self, user_id):
        return AuthenticatedUser.objects.filter(id=user_id).values_list("phone", flat=True).get() is not None


def _reraise_set_phone_error(e: ValidationError):
    if "phone" in e.error_dict:
        phone_errors = e.error_dict["phone"]
        phone_errors_codes = set(ex.code for ex in phone_errors)

        if "unique" in phone_errors_codes:
            raise PhoneNumberAlreadyExistsError

        if "invalid" in phone_errors_codes:
            raise IncorrectPhoneNumberError
    raise e


class FavoriteRepository(IFavoriteRepository):
    def is_favorite_for(self, user_id: int, favorite_user_id: int) -> bool:
        return Favorite.objects.filter(user__id=user_id, favorite_user__id=favorite_user_id).exists()

    def add_favorite_for(self, user_id: int, favorite_user_id: int) -> None:
        Favorite.objects.create(user_id=user_id, favorite_user_id=favorite_user_id)

    def remove_favorite_of(self, user_id: int, favorite_user_id: int) -> bool:
        deleted, _ = Favorite.objects.filter(user__id=user_id, favorite_user__id=favorite_user_id).delete()
        return deleted

    def get_favorites_usernames_of_user(self, user_id: int) -> List[str]:
        return list(Favorite.objects.filter(user__id=user_id).values_list("favorite_user__username", flat=True))
