from typing import List

from ninja import Schema
from ninja.orm import create_schema

from app.internal.users.db.models import AuthenticatedUser

UserSchema = create_schema(
    AuthenticatedUser, fields=["id", "telegram_id", "first_name", "last_name", "username", "phone"]
)


class UserSchemaOut(UserSchema):
    ...


class FavoriteUsernameSchemaIn(Schema):
    username: str


class FavoritesUsernamesSchemaOut(Schema):
    favorites: List[str] = []


class PhoneSetSchemaIn(Schema):
    phone: str
