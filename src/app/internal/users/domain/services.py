from typing import List, Tuple

from app.internal.bot.services.exceptions import (
    FavoriteAlreadyAdded,
    FavoriteNotAddedError,
    FavoriteNotFoundError,
    HasNotPhoneError,
    IncorrectPhoneNumberError,
    IncorrectUserPasswordError,
    SelfFavoriteError,
)
from app.internal.users.db.models import AuthenticatedUser
from app.internal.users.domain.entities import FavoritesUsernamesSchemaOut, UserSchemaOut


class IUserRepository:
    def get_user_by_telegram(self, telegram_id, fields: List[str]) -> List:
        ...

    def save_user(
        self, telegram_id: int, first_name: str, last_name: str, username: str, password: str
    ) -> Tuple[AuthenticatedUser, bool]:
        ...

    def set_phone(self, user_id: int, phone_number: str):
        ...

    def get_user_by_id(self, user_id: int) -> AuthenticatedUser:
        ...

    def find_user_by_username(self, username: str) -> AuthenticatedUser:
        ...

    def get_user_by_telegram_id(self, telegram_id: str) -> AuthenticatedUser:
        ...

    def get_user_id_by_username(self, username: str) -> int:
        ...

    def has_phone(self, user_id):
        ...


class IFavoriteRepository:
    def is_favorite_for(self, user_id: int, favorite_user_id: int) -> bool:
        ...

    def add_favorite_for(self, user_id: int, favorite_user_id: int) -> None:
        ...

    def remove_favorite_of(self, user_id: int, favorite_user_id: int) -> bool:
        ...

    def get_favorites_usernames_of_user(self, user_id: int) -> List[str]:
        ...


class UserService:
    def __init__(self, user_repo: IUserRepository):
        self.user_repo = user_repo

    def get_user_fields_by_telegram(self, telegram_id, fields=None):
        return self.user_repo.get_user_by_telegram(telegram_id, fields)

    def save_user(self, telegram_id, first_name, last_name, username, password):
        last_name = "" if last_name is None else last_name

        _, created = self.user_repo.save_user(telegram_id, first_name, last_name, username, password)

        return created

    def set_user_phone(self, user_id, phone_number: str):
        if phone_number is None:
            raise IncorrectPhoneNumberError

        phone_number = phone_number.strip()
        if len(phone_number) == 0:
            raise IncorrectPhoneNumberError

        self.user_repo.set_phone(user_id, phone_number)

    def get_user_info(self, user_id: int):
        if not self.user_repo.has_phone(user_id):
            raise HasNotPhoneError
        user = self.user_repo.get_user_by_id(user_id)
        return UserSchemaOut.from_orm(user)

    def update_password(self, telegram_id, password, old_password):
        user = self.user_repo.get_user_by_telegram_id(telegram_id)

        if not user.check_password(old_password):
            raise IncorrectUserPasswordError

        user.set_password(password)
        user.save()


class FavoriteService:
    def __init__(self, user_repo: IUserRepository, favorite_repo: IFavoriteRepository):
        self.user_repo = user_repo
        self.favorite_repo = favorite_repo

    def add_favorite_by_username(self, user_id, favorite_username):
        try:
            favorite_user_id = self.user_repo.get_user_id_by_username(favorite_username)
        except AuthenticatedUser.DoesNotExist:
            raise FavoriteNotFoundError

        if favorite_user_id == user_id:
            raise SelfFavoriteError

        if self.favorite_repo.is_favorite_for(user_id, favorite_user_id):
            raise FavoriteAlreadyAdded

        self.favorite_repo.add_favorite_for(user_id, favorite_user_id)

    def remove_favorite_by_username(self, user_id, favorite_username):
        try:
            favorite_user_id = self.user_repo.get_user_id_by_username(favorite_username)
        except AuthenticatedUser.DoesNotExist:
            raise FavoriteNotFoundError

        deleted = self.favorite_repo.remove_favorite_of(user_id, favorite_user_id)
        if deleted == 0:
            raise FavoriteNotAddedError

    def get_favorites_of_user(self, user_id) -> FavoritesUsernamesSchemaOut:
        if user_id is None:
            raise TypeError
        if not self.user_repo.has_phone(user_id):
            raise HasNotPhoneError
        usernames = self.favorite_repo.get_favorites_usernames_of_user(user_id)
        return FavoritesUsernamesSchemaOut(favorites=usernames)

    def is_favorite_for(self, user_id, other_user_id):
        if user_id is None or other_user_id is None:
            raise TypeError

        return self.favorite_repo.is_favorite_for(user_id, other_user_id)
