from datetime import datetime
from decimal import Decimal

from ninja import Schema


class TransactionSchema(Schema):
    sender_username: str
    recipient_username: str
    money: Decimal
    date: datetime

    def __init__(self, sender_username, recipient_username, money, date):
        super().__init__(sender_username=sender_username, recipient_username=recipient_username, money=money, date=date)

    def __eq__(self, other):
        return (
            self.sender_username == other.sender_username
            and self.recipient_username == other.recipient_username
            and self.money == other.money
            and self.date == other.date
        )

    def __str__(self):
        return (
            f"TransactionDto(sender_username={self.sender_username}, "
            f"recipient_username={self.recipient_username}, "
            f"money={self.money}, "
            f"date={self.date})"
        )

    def __repr__(self):
        return str(self)


class CardBalanceSchemaOut(Schema):
    balance: float = 0


class TransferMoneySchemaIn(Schema):
    to: str
    source: str | int | None
    money: float
