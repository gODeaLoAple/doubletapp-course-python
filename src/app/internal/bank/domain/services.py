from datetime import datetime
from typing import List

from django.db.models import QuerySet

from app.internal.bank.db.models import BankAccount
from app.internal.bank.domain.entities import CardBalanceSchemaOut, TransactionSchema
from app.internal.bot.services.exceptions import (
    HasNotPhoneError,
    IncorrectTransferMoneyError,
    NotEnoughMoneyError,
    SelfTransferError,
    TargetBankAccountNotFoundError,
    TargetBankAccountNotSpecifiedError,
    TargetNotFavoriteError,
    UserBankAccountNotFoundError,
    UserBankAccountNotSpecifiedError,
    UserHasNotBankAccountError,
)
from app.internal.users.domain.services import FavoriteService, IUserRepository


class ICardRepository:
    def get_card_balance(self, user_id: int, card_number: str) -> float:
        pass

    def get_cards_names_of_account(self, bank_account_id: int) -> List[str]:
        pass


class IBankAccountRepository:
    def get_bank_account_id_by_card_or_account(self, user_id: int, card_or_account_id: str) -> int:
        pass

    def get_all_by_username_or_id_or_card(self, username_or_bank_or_card: str, limit: int = None) -> List[BankAccount]:
        pass

    def get_all_by_user_or_id_or_card(self, user_id: int, id_or_card: str, limit: int = None) -> List[BankAccount]:
        pass

    def get_all_by_user(self, user_id: int, limit: int = None) -> List[BankAccount]:
        ...


class ITransferMoneyRepository:
    def transfer_money(self, user_bank_account, recipient_bank_account, money):
        pass


class CardService:
    def __init__(self, card_repo: ICardRepository):
        self.card_repo = card_repo

    def get_card_balance(self, user_id, card_number):
        balance = self.card_repo.get_card_balance(user_id, card_number)
        return CardBalanceSchemaOut(balance=balance)


class ITransactionRepository:
    def get_last_transactions(self, bank_account_id: int, start_date: datetime) -> List[TransactionSchema]:
        ...

    def get_recipients_usernames(self, user_id: int) -> QuerySet[str]:
        ...

    def get_senders_usernames(self, user_id: int) -> QuerySet[str]:
        ...


class TransactionService:
    def __init__(self, transaction_repo: ITransactionRepository):
        self.transaction_repo = transaction_repo

    def get_transaction_history(self, user_id: int) -> List[str]:
        recipients = self.transaction_repo.get_recipients_usernames(user_id)
        senders = self.transaction_repo.get_senders_usernames(user_id)
        return list(recipients.union(senders))

    def get_last_transactions(self, bank_account_id: int, start_date: datetime) -> List[TransactionSchema]:
        return self.transaction_repo.get_last_transactions(bank_account_id, start_date)


class TransferService:
    def __init__(
        self,
        user_repo: IUserRepository,
        bank_account_repo: IBankAccountRepository,
        transfer_money_repo: ITransferMoneyRepository,
        favorite_service: FavoriteService,
    ):
        self.user_repo = user_repo
        self.bank_account_repo = bank_account_repo
        self.transfer_money_repo = transfer_money_repo
        self.favorite_service = favorite_service

    def transfer_money_to_bank_account(self, user_id, target_description: str, money, user_bank_or_card=None):
        if not self.user_repo.has_phone(user_id):
            raise HasNotPhoneError

        if money <= 0:
            raise IncorrectTransferMoneyError

        recipient_bank_account = self._resolve_target_bank_account(target_description)

        if not self.favorite_service.is_favorite_for(user_id, recipient_bank_account.user_id):
            raise TargetNotFavoriteError

        user_bank_account = self._resolve_user_bank_account(user_id, user_bank_or_card)

        if recipient_bank_account.id == user_bank_account.id:
            raise SelfTransferError

        if user_bank_account.money < money:
            raise NotEnoughMoneyError

        self.transfer_money_repo.transfer_money(user_bank_account, recipient_bank_account, money)

    def _resolve_target_bank_account(self, username_or_bank_or_card):
        result = self.bank_account_repo.get_all_by_username_or_id_or_card(username_or_bank_or_card, limit=2)

        if not result:
            raise TargetBankAccountNotFoundError

        if len(result) > 1:
            raise TargetBankAccountNotSpecifiedError

        return result[0]

    def _resolve_user_bank_account(self, user_id, user_bank_or_card=None):
        if user_bank_or_card:
            result = self.bank_account_repo.get_all_by_user_or_id_or_card(user_id, user_bank_or_card, limit=2)
            result = result

            if not result:
                raise UserBankAccountNotFoundError
        else:
            result = self.bank_account_repo.get_all_by_user(user_id, limit=2)

            if not result:
                raise UserHasNotBankAccountError

        if len(result) > 1:
            raise UserBankAccountNotSpecifiedError

        return result[0]


class StatementService:
    def __init__(
        self,
        bank_account_repo: IBankAccountRepository,
        card_repo: ICardRepository,
        transaction_service: TransactionService,
    ):
        self.card_repo = card_repo
        self.transaction_service = transaction_service
        self.bank_account_repo = bank_account_repo

    def get_statement(self, user_id: int, card_or_account_id: str, start_date: datetime):
        bank_account_id = self.bank_account_repo.get_bank_account_id_by_card_or_account(user_id, card_or_account_id)
        transactions = self.transaction_service.get_last_transactions(bank_account_id, start_date)
        cards_names = self.card_repo.get_cards_names_of_account(bank_account_id)
        return bank_account_id, cards_names, transactions
