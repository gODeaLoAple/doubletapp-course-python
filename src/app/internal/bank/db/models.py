from django.db import models
from django.utils import timezone

from app.internal.users.db.models import AuthenticatedUser


class BankAccount(models.Model):
    id = models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name="id")
    user = models.ForeignKey(AuthenticatedUser, on_delete=models.CASCADE, verbose_name="user")
    money = models.DecimalField(max_digits=10, decimal_places=2, verbose_name="money")

    class Meta:
        verbose_name = "BankAccount"
        verbose_name_plural = "BankAccounts"


class Card(models.Model):
    id = models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name="id")
    bank_account = models.ForeignKey(BankAccount, on_delete=models.CASCADE, verbose_name="bank_account")
    name = models.CharField(unique=True, max_length=16, blank=False, verbose_name="name", db_index=True)

    class Meta:
        verbose_name = "Card"
        verbose_name_plural = "Cards"


class Transaction(models.Model):
    id = models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name="id")
    sender_account = models.ForeignKey(
        BankAccount, on_delete=models.CASCADE, related_name="sender_account", verbose_name="sender_account"
    )
    recipient_account = models.ForeignKey(
        BankAccount, on_delete=models.CASCADE, related_name="recipient_account", verbose_name="recipient_account"
    )
    money = models.DecimalField(max_digits=10, decimal_places=2, verbose_name="money")
    date = models.DateTimeField(default=timezone.now, verbose_name="date")

    class Meta:
        verbose_name = "Transaction"
        verbose_name_plural = "Transactions"
