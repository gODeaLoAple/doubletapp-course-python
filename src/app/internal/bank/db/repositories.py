from datetime import datetime
from typing import List

from django.db import transaction
from django.db.models import F, Q, QuerySet

from app.internal.bank.db.models import BankAccount, Card, Transaction
from app.internal.bank.domain.entities import TransactionSchema
from app.internal.bank.domain.services import (
    IBankAccountRepository,
    ICardRepository,
    ITransactionRepository,
    ITransferMoneyRepository,
)
from app.internal.bot.services.exceptions import UserCardNotFoundError
from app.internal.utils.parse import parse_telegram_username, try_parse_int


class CardRepository(ICardRepository):
    def get_card_balance(self, user_id: int, card_number: str) -> float:
        money = (
            BankAccount.objects.filter(user_id=user_id, card__name=card_number).values_list("money", flat=True).first()
        )
        if money is None:
            raise UserCardNotFoundError
        return money

    def get_cards_names_of_account(self, bank_account_id: int) -> List[str]:
        return list(Card.objects.filter(bank_account__id=bank_account_id).values_list("name", flat=True))


class BankAccountRepository(IBankAccountRepository):
    def get_bank_account_id_by_card_or_account(self, user_id: int, card_or_account_id: str) -> int:
        f = Q(card__name=card_or_account_id)
        account_id, parsed = try_parse_int(card_or_account_id)
        if parsed:
            f |= Q(id=account_id)
        f &= Q(user__id=user_id)
        return BankAccount.objects.filter(f).distinct().values_list("id", flat=True).get()

    def get_all_by_username_or_id_or_card(self, username_or_bank_or_card: str, limit: int = None) -> List[BankAccount]:
        assert limit is None or limit > 0
        account_id, parsed = try_parse_int(username_or_bank_or_card)
        query_filter = Q(user__username=parse_telegram_username(username_or_bank_or_card))
        query_filter |= Q(card__name=username_or_bank_or_card)
        query_filter = query_filter | Q(id=account_id) if parsed else query_filter
        query = BankAccount.objects.select_related("user").filter(query_filter)
        return list(query) if limit is None else list(query[:limit])

    def get_all_by_user_or_id_or_card(self, user_id: int, id_or_card: str, limit: int = None) -> List[BankAccount]:
        assert limit is None or limit > 0
        account_id, parsed = try_parse_int(id_or_card)
        query_filter = Q(card__name=id_or_card)
        query_filter = query_filter | Q(id=account_id) if parsed else query_filter
        query_filter &= Q(user__id=user_id)
        query = BankAccount.objects.filter(query_filter)
        return list(query) if limit is None else list(query[:limit])

    def get_all_by_user(self, user_id: int, limit: int = None) -> List[BankAccount]:
        assert limit is None or limit > 0
        query = BankAccount.objects.filter(user__id=user_id)
        return list(query) if limit is None else list(query[:limit])


class TransactionRepository(ITransactionRepository):
    def get_last_transactions(self, bank_account_id: int, start_date: datetime) -> List[TransactionSchema]:
        result = (
            Transaction.objects.filter(Q(date__gte=start_date))
            .filter(Q(sender_account__id=bank_account_id) | Q(recipient_account__id=bank_account_id))
            .order_by("-date")
            .values_list("sender_account__user__username", "recipient_account__user__username", "money", "date")
        )

        return [TransactionSchema(*values) for values in result]

    def get_recipients_usernames(self, user_id: int) -> QuerySet[str]:
        return Transaction.objects.filter(sender_account__user__id=user_id).values_list(
            "recipient_account__user__username", flat=True
        )

    def get_senders_usernames(self, user_id: int) -> QuerySet[str]:
        return Transaction.objects.filter(recipient_account__user__id=user_id).values_list(
            "sender_account__user__username", flat=True
        )


class TransferMoneyRepository(ITransferMoneyRepository):
    def transfer_money(self, user_bank_account: BankAccount, recipient_bank_account: BankAccount, money: float):
        with transaction.atomic():
            recipient_bank_account.money = F("money") + money
            user_bank_account.money = F("money") - money
            BankAccount.objects.bulk_update([recipient_bank_account, user_bank_account], ["money"], 2)
            Transaction.objects.create(
                sender_account_id=user_bank_account.id, recipient_account_id=recipient_bank_account.id, money=money
            )
