from ninja import NinjaAPI

from app.internal.auth.presentation.middleware.http_jwt_auth import HTTPJWTAuth
from app.internal.bank.db.repositories import BankAccountRepository, CardRepository, TransferMoneyRepository
from app.internal.bank.domain.services import CardService, TransferService
from app.internal.bank.presentation.handlers import CardHandlers, MoneyHandlers
from app.internal.bank.presentation.routers import add_bank_routers
from app.internal.users.db.repositories import FavoriteRepository, UserRepository
from app.internal.users.domain.services import FavoriteService


def get_api():
    api = NinjaAPI(
        title="DT.EDU.BACKEND",
        version="1.0.0",
        auth=[HTTPJWTAuth()],
    )
    configure_bank_api(api)
    return api


def configure_bank_api(api: NinjaAPI):
    card_repo = CardRepository()
    bank_account_repo = BankAccountRepository()
    card_service = CardService(card_repo)
    card_handlers = CardHandlers(card_service)
    user_repo = UserRepository()
    favorite_repo = FavoriteRepository()
    favorite_service = FavoriteService(user_repo, favorite_repo)
    transfer_money_repo = TransferMoneyRepository()
    transfer_money_service = TransferService(user_repo, bank_account_repo, transfer_money_repo, favorite_service)
    money_handlers = MoneyHandlers(transfer_money_service)
    add_bank_routers(api, card_handlers, money_handlers)
