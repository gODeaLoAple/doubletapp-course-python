from django.contrib import admin
from django.urls import reverse
from django.utils.html import format_html

from app.internal.bank.db.models import BankAccount, Card, Transaction


@admin.register(BankAccount)
class BankAccountAdmin(admin.ModelAdmin):
    list_display = ("id", "view_user", "money")

    def view_user(self, obj):
        url = reverse("admin:app_user_change", args=[obj.user.id])
        return format_html('<a href="{}"> {} ({})</a>', url, obj.user.username, obj.user.id)

    view_user.short_description = "User"


@admin.register(Card)
class CardAdmin(admin.ModelAdmin):
    list_display = ("id", "view_bank_account", "name", "money")

    def view_bank_account(self, obj):
        account_id = obj.bank_account.id
        url = reverse("admin:app_bankaccount_change", args=[account_id])
        from django.utils.html import format_html

        return format_html('<a href="{}"> Bank account ({})</a>', url, account_id)

    view_bank_account.short_description = "Bank account"

    def money(self, obj):
        return obj.bank_account.money


@admin.register(Transaction)
class TransactionAdmin(admin.ModelAdmin):
    list_display = ("id", "view_sender_account", "view_recipient_account", "money", "date")
    list_filter = ("sender_account", "recipient_account", "date")

    def view_sender_account(self, obj):
        url = reverse("admin:app_bankaccount_change", args=[obj.sender_account.id])
        return format_html('<a href="{}"> Bank account ({})</a>', url, obj.sender_account.id)

    view_sender_account.short_description = "Sender account"

    def view_recipient_account(self, obj):
        url = reverse("admin:app_bankaccount_change", args=[obj.recipient_account.id])
        return format_html('<a href="{}"> Bank account ({})</a>', url, obj.recipient_account.id)

    view_recipient_account.short_description = "Recipient account"
