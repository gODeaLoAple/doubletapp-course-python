from ninja import NinjaAPI, Router

from app.internal.auth.presentation.middleware.http_jwt_auth import HTTPJWTAuth
from app.internal.bank.domain.entities import CardBalanceSchemaOut
from app.internal.bank.presentation.handlers import CardHandlers, MoneyHandlers
from app.internal.utils.http_responses import Forbidden, Ok, Unauthorized


def get_cards_router(card_handlers: CardHandlers):
    router = Router(tags=["cards"])

    router.add_api_operation(
        "money/{code}",
        ["GET"],
        card_handlers.get_money,
        response={200: CardBalanceSchemaOut, 401: Unauthorized, 403: Forbidden},
        auth=[HTTPJWTAuth()],
    )

    return router


def get_money_router(money_handlers: MoneyHandlers):
    router = Router(tags=["money"])

    router.add_api_operation(
        "transfer/",
        ["POST"],
        money_handlers.transfer,
        response={200: Ok, 401: Unauthorized, 403: Forbidden},
        auth=[HTTPJWTAuth()],
    )

    return router


def add_bank_routers(api: NinjaAPI, cards_handlers: CardHandlers, money_handlers: MoneyHandlers):
    api.add_router("cards", get_cards_router(cards_handlers))
    api.add_router("money", get_money_router(money_handlers))
