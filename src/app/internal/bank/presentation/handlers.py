from django.http import HttpRequest
from ninja import Body, Path

from app.internal.bank.domain.entities import CardBalanceSchemaOut, TransferMoneySchemaIn
from app.internal.bank.domain.services import CardService, TransferService
from app.internal.utils.http_responses import Ok
from app.internal.utils.rest_exceptions import UnauthorizedError


class CardHandlers:
    def __init__(self, card_service: CardService):
        self.card_service = card_service

    def get_money(self, request: HttpRequest, code: str = Path(...)) -> CardBalanceSchemaOut:
        user = request.user
        if user is None:
            raise UnauthorizedError()
        return self.card_service.get_card_balance(user.id, code)


class MoneyHandlers:
    def __init__(self, transfer_service: TransferService):
        self.transfer_service = transfer_service

    def transfer(self, request: HttpRequest, data: TransferMoneySchemaIn = Body(...)) -> Ok:
        user = request.user
        if user is None:
            raise UnauthorizedError()
        self.transfer_service.transfer_money_to_bank_account(user.id, data.to, data.money, data.source)
        return Ok()
