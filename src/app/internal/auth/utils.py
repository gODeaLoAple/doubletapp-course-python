import jwt
from django.utils import timezone

from config import settings

JWT_ALGORITHM = "HS256"
JWT_SECRET_KEY = settings.JWT_SECRET
JWT_ACCESS_TOKEN_EXPIRED = settings.JWT_ACCESS_TOKEN_EXPIRED
JWT_REFRESH_TOKEN_EXPIRED = settings.JWT_REFRESH_TOKEN_EXPIRED


def try_parse_token(raw_token: str | None):
    if raw_token is None:
        return False, None
    try:
        return True, jwt.decode(raw_token, JWT_SECRET_KEY, algorithms=[JWT_ALGORITHM], options=dict(verify_exp=False))
    except:
        return False, None


def is_token_has_expired(token):
    expired_date = token.expires_at
    if expired_date is None:
        return True
    return expired_date < timezone.now()


def generate_access_token(user_id, expired_delay):
    dt = timezone.now() + expired_delay
    token = jwt.encode(payload={"id": user_id, "exp": int(dt.timestamp())}, key=JWT_SECRET_KEY, algorithm=JWT_ALGORITHM)
    return token


def generate_refresh_token(token_id, refresh_expires_delay):
    dt = (timezone.now() + refresh_expires_delay).timestamp()
    return jwt.encode(payload={"id": token_id, "exp": int(dt)}, key=JWT_SECRET_KEY, algorithm=JWT_ALGORITHM)
