from datetime import datetime

from app.internal.auth.db.models import IssuedToken
from app.internal.auth.domain.services import ITokenRepository


class TokenRepository(ITokenRepository):
    def revoke_user_tokens(self, user_id: int) -> None:
        IssuedToken.objects.filter(user__id=user_id).update(revoked=True)

    def find_token_with_user_by_id(self, token_id: int) -> IssuedToken:
        return IssuedToken.objects.filter(id=token_id).select_related("user").first()

    def create_token(self, user_id: int, refresh_expires_at: datetime) -> IssuedToken:
        return IssuedToken.objects.create(expires_at=refresh_expires_at, user_id=user_id)
