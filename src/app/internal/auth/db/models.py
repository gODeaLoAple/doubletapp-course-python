from django.db import models

from app.internal.users.db.models import AuthenticatedUser


class IssuedToken(models.Model):
    id = models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name="id")
    expires_at = models.DateTimeField()
    user = models.ForeignKey(AuthenticatedUser, related_name="refresh_tokens", on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    revoked = models.BooleanField(default=False)
