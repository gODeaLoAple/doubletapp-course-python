from ninja import NinjaAPI

from app.internal.auth.db.repositories import TokenRepository
from app.internal.auth.domain.services import AuthService
from app.internal.auth.presentation.handlers import AuthHandlers
from app.internal.auth.presentation.routers import add_auth_router
from app.internal.users.db.repositories import UserRepository


def get_api():
    api = NinjaAPI(
        title="DT.EDU.BACKEND",
        version="1.0.0",
        auth=None,
    )
    configure_auth_api(api)
    return api


def configure_auth_api(api: NinjaAPI):
    user_repo = UserRepository()
    token_repo = TokenRepository()
    auth_service = AuthService(user_repo, token_repo)
    auth_handlers = AuthHandlers(auth_service)
    add_auth_router(api, auth_handlers)
