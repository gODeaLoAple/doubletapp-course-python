from ninja import Schema


class LoginSchemaIn(Schema):
    username: str
    password: str


class JWTokensSchemaOut(Schema):
    refresh_token: str
    access_token: str


class JWTokensSchemaIn(Schema):
    refresh_token: str
