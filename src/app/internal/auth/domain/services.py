from datetime import datetime, timedelta

from django.utils import timezone

from app.internal.auth.db.models import IssuedToken
from app.internal.auth.domain.entities import JWTokensSchemaOut
from app.internal.auth.utils import generate_access_token, generate_refresh_token, is_token_has_expired, try_parse_token
from app.internal.bot.services.exceptions import (
    AuthenticatedUserNotFoundError,
    RefreshTokenExpiredError,
    RefreshTokenHasBeenRevokedError,
    RefreshTokenNotRecognizedError,
)
from app.internal.users.db.models import AuthenticatedUser
from app.internal.users.domain.services import IUserRepository
from config import settings


class ITokenRepository:
    def revoke_user_tokens(self, user_id: int) -> None:
        ...

    def find_token_with_user_by_id(self, token_id: int) -> IssuedToken:
        ...

    def create_token(self, user_id: int, refresh_expires_at: datetime) -> IssuedToken:
        ...


JWT_ACCESS_TOKEN_EXPIRED = settings.JWT_ACCESS_TOKEN_EXPIRED
JWT_REFRESH_TOKEN_EXPIRED = settings.JWT_REFRESH_TOKEN_EXPIRED


class AuthService:
    def __init__(self, user_repo: IUserRepository, token_repo: ITokenRepository):
        self.user_repo = user_repo
        self.token_repo = token_repo

    def login(self, username: str, password: str) -> JWTokensSchemaOut:
        if username is None or password is None:
            raise ValueError
        user = self.user_repo.find_user_by_username(username)
        if user is None or not user.check_password(password):
            raise AuthenticatedUserNotFoundError
        self.token_repo.revoke_user_tokens(user.id)
        return self.generate_tokens(user)

    def refresh(self, raw_refresh_token: str | None):
        success, refresh_token = try_parse_token(raw_refresh_token)
        if not success:
            raise RefreshTokenNotRecognizedError

        issued_token = self.token_repo.find_token_with_user_by_id(refresh_token["id"])
        if issued_token is None:
            raise RefreshTokenNotRecognizedError

        user = issued_token.user
        self.token_repo.revoke_user_tokens(user.id)
        if issued_token.revoked:
            raise RefreshTokenHasBeenRevokedError
        if is_token_has_expired(issued_token):
            raise RefreshTokenExpiredError

        return self.generate_tokens(user)

    def generate_tokens(self, user: AuthenticatedUser) -> JWTokensSchemaOut:
        access_jwt_token = generate_access_token(user.id, timedelta(seconds=JWT_ACCESS_TOKEN_EXPIRED))
        refresh_expires_delay = timedelta(seconds=JWT_REFRESH_TOKEN_EXPIRED)
        refresh_expires_at = timezone.now() + refresh_expires_delay
        entity = self.token_repo.create_token(user.id, refresh_expires_at)
        refresh_jwt_token = generate_refresh_token(entity.id, refresh_expires_delay)
        return JWTokensSchemaOut(refresh_token=refresh_jwt_token, access_token=access_jwt_token)
