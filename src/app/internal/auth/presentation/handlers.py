from django.http import HttpRequest, JsonResponse
from ninja import Body

from app.internal.auth.domain.entities import JWTokensSchemaIn, LoginSchemaIn
from app.internal.auth.domain.services import AuthService
from app.internal.bot.services.exceptions import (
    AuthenticatedUserNotFoundError,
    RefreshTokenExpiredError,
    RefreshTokenHasBeenRevokedError,
    RefreshTokenNotRecognizedError,
)


class AuthHandlers:
    def __init__(self, auth_service: AuthService):
        self.auth_service = auth_service

    def login(self, request: HttpRequest, model: LoginSchemaIn = Body(...)):
        try:
            return self.auth_service.login(model.username, model.password)
        except ValueError:
            return JsonResponse({"detail": "username and password must be present"}, status=400)
        except AuthenticatedUserNotFoundError:
            return JsonResponse({"detail": "User not found"}, status=401)

    def refresh(self, request: HttpRequest, model: JWTokensSchemaIn = Body(...)):
        try:
            return self.auth_service.refresh(model.refresh_token)
        except ValueError:
            return JsonResponse({"detail": 'key "refresh_token" must be present'}, status=400)
        except RefreshTokenNotRecognizedError:
            return JsonResponse({"detail": "Refresh token is not recognized"}, status=400)
        except RefreshTokenHasBeenRevokedError:
            return JsonResponse({"detail": "Refresh token has been revoked"}, status=400)
        except RefreshTokenExpiredError:
            return JsonResponse({"detail": "Refresh token is expired"}, status=400)
