from datetime import datetime

from django.contrib.auth import get_user_model
from django.http import HttpRequest
from ninja.security import HttpBearer

from app.internal.auth.utils import try_parse_token


class HTTPJWTAuth(HttpBearer):
    def authenticate(self, request: HttpRequest, token: str):
        try:
            success, payload = try_parse_token(token)
            if not success:
                return None
            expired_date = datetime.fromtimestamp(payload["exp"])
            if expired_date < datetime.now():
                return None
            request.user = get_user_model().objects.get(id=payload["id"])
        except:
            return None
        return token
