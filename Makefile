include .env

migrate:
	docker-compose run --rm web python manage.py migrate $(if $m, api $m,)

makemigrations:
	sudo docker-compose run --rm web python manage.py makemigrations

createsuperuser:
	sudo docker-compose run --rm web python manage.py createsuperuser

collectstatic:
	sudo docker-compose run --rm web python manage.py collectstatic --no-input

build:
	docker-compose build

dev:
	docker-compose up -d

push:
	docker push ${IMAGE_APP}

pull:
	docker pull ${IMAGE_APP}


command:
	sudo docker-compose run --rm web python manage.py ${c}

shell:
	sudo docker-compose run --rm web python manage.py shell

debug:
	sudo docker-compose run --rm web python manage.py debug

piplock:
	pipenv install
	sudo chown -R ${USER} Pipfile.lock

lint:
	isort .
	flake8 --config setup.cfg
	black --config pyproject.toml .

check_lint:
	pipenv run isort --check --diff .
	pipenv run flake8 --config setup.cfg
	pipenv run black --check --config pyproject.toml .

test:
	docker-compose run --rm web pytest tests

test_unit:
	docker-compose run --rm web pytest tests/unit

test_integration:
	docker-compose run --rm web pytest tests/integration

test_smoke:
	docker-compose run --rm web pytest tests/smoke

test_performance:
	docker run --rm -v $(pwd):/var/loadtest -it direvius/yandex-tank
	
build_image:
	sudo docker image build -t dt-app:dev .

down:
	docker-compose down

db_init:
	sudo docker-compose run --rm db

docker_check_lint:
	docker run --rm ${IMAGE_APP}
	docker-compose run web isort --check --diff .
	docker-compose run web flake8 --config setup.cfg
	docker-compose run web black --check --config pyproject.toml .