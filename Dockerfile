FROM python:3.10-alpine


ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV PIPENV_DEV 1

WORKDIR /app

RUN apk update && apk add postgresql-dev gcc python3-dev musl-dev
COPY Pipfile .
COPY Pipfile.lock .
RUN pip install --upgrade pip && pip install pipenv
RUN pipenv update && pipenv install --system --deploy --ignore-pipfile

COPY setup.cfg ./
COPY pyproject.toml ./
COPY ./src/ ./